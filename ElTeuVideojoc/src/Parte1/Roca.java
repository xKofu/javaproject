package Parte1;

import Core.Field;
import Core.Sprite;

public class Roca extends Sprite {

	public int accionesDisponibles;
	
	public static int contador = 0;
	int id;
	
	public Roca(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.contador++;
		this.id = contador;
		this.name = name + id;
	}
	
	public Roca(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int acc) {
		super(name, x1, y1, x2, y2, angle, path, f);
		
		this.accionesDisponibles = acc;
		this.contador++;
		this.id = contador;
		this.name = name + id;
	}

	public Roca(int x1, int y1, int x2, int y2, Field f, int acc) {
		super("Roca", x1, y1, x2, y2, 0, "resources/kuentin.jpg", f);
		
		this.accionesDisponibles = acc;
		this.contador++;
		this.id = contador;
		this.name = name + id;
	}
	
	public Roca(int x1, int y1, int size, Field f) {
		
		super("Roca", x1, y1, x1 + size, y1 + size, 0, "resources/kuentin.jpg", f);
		
		this.accionesDisponibles = 50;
		this.contador++;
		this.id = contador;
		this.name = name + id;
	}
	
	public Roca() {
		
		super("Roca", 0, 0, 50, 50, 0, "resources/kuentin.jpg", Juego1.f);
		this.accionesDisponibles = 50;
		this.contador++;
		this.id = contador;
		this.name = name + id;
	}
	
}
