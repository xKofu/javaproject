package Parte1;

import Core.Field;
import Core.Sprite;
import Core.Window;


public class Juego1 {

	static Field f = new Field();
	
	static Window w = new Window(f);

	public static void main(String[] args) throws InterruptedException {
				
		//Sprite roca = new Sprite("Roca", 50, 50, 500, 450, 0, "resources/kuentin.jpg", f);
		//Sprite roca2 = new Sprite("Roca2", 50, 460, 500, 860, 0, "resources/kuentin.jpg", f);
		
		Roca roca = new Roca();
		Roca roca2 = new Roca("Roca", 0, 60, 150, 210, 0, "resources/kuentin.jpg", f, 50);
		Roca roca3 = new Roca("Roca", 160, 60, 310, 210, 0, "resources/kuentin.jpg", f);
		Roca roca4 = new Roca(320, 60, 470, 210, f, 50);
		Roca roca5 = new Roca(480, 60, 150, f);
		
		boolean salir = false;
		
		System.out.println(roca.name + " " + roca2.name + " " + roca3.id + " " + roca4.id + " " + roca5.id );
		
		while (!salir) {
			f.draw();
			
			Thread.sleep(30);
		}
	}

}
