package Parte3;

import java.util.Set;

import Core.Field;
import Core.Window;
import Core.Sprite;
import Parte1.Roca;
import Parte2.pj;

/*

A implementar en el juego
	- Sistema de vidas para obstaculos y pinchos
		· 3 vidas maximas
		· Se pueden recuperar con vidas que nos encontramos en el mapa
		· Si se pierden las 3 vidas y nos quedamos a 0, acaba el juego
	- Sistema de disparo activar botones
		. Se conseguiran plataformas nuevas al acertar en botones
		· Los botones tendran 2 graficos conforme estan apagados o encendidos
	- Obstaculos nuevos
		· cañon que dispara siempre en la misma direccion y al mismo ritmo
			· Se pueden destruir con los disparos
	- Sistema de finalizacion del juego
		· Un portal en una localizacion concreta del mapa que solo es posible de usar si conseguimos todas las monedas
		· Sale mensaje en el centro de la pantalla igual que si acaba la partida por Game Over pero felicitando al Jugador

*/

public class Juego4 {

	static Field f = new Field();
	
	static Window w = new Window(f, 1600, 1100);
		
	static boolean moneda1 = true;
	static boolean moneda2 = true;
	static boolean moneda3 = true;
	
	public static pj Pers;
	public static Sprite MonedaHueca1;
	public static Sprite MonedaHueca2;
	public static Sprite MonedaHueca3;
	
	public static Roca plat1;
	public static Roca plat2;
	public static Roca plat3;
	public static Roca plat4;
	public static Roca plat5;
	public static Roca plat6;
	public static Roca plat7;
	public static Roca plat8;
	public static Roca plat9;
	public static Roca plat10;
	
	public static Sprite Pincho1;
	public static Sprite Pincho2;
		
	public static Sprite Moneda1;
	public static Sprite Moneda2;
	public static Sprite Moneda3;
	
	public static Sprite MensajeFin;
	
	public static void main(String[] args) throws InterruptedException {
		
		MensajeFin = new Sprite("msg", 1700, 1500, 1850, 1550, 0, "resources/mensaje.png", f);
		
		IniciarJuego();
				
		boolean salir = false;
					
		while (!salir) {
			f.draw();
			
			Thread.sleep(30);
			
			Set<Character> Teclas = input();
			Pers.mov(Teclas);
			Set<Character> TeclasD = inputd();
			
			if (TeclasD.contains('w')) {
				Pers.Salto();
			}
			if (TeclasD.contains(' ')) {
				Pers.Disparar();
			}
			if (TeclasD.contains('p')) {
				BorrarTodo();
				IniciarJuego();
				MensajeFin.x1 = 1700;
				MensajeFin.x2 = 1850;
				MensajeFin.y1 = 1500;
				MensajeFin.y2 = 1550;
			
			}
			
			Monedas();
			Log();
			
			if (Pers.muerto) {
				BorrarTodo();
				MensajeFin.x1 = 700;
				MensajeFin.x2 = 850;
				MensajeFin.y1 = 500;
				MensajeFin.y2 = 550;
			}
			
		}
		
	}

	private static void Log() {
		
		System.out.println("Monedas: " + Pers.monedas);
		System.out.println("Localizacion: " + Pers.x1 + " " + Pers.x2 + " " + Pers.y1 + " " + Pers.y2);
		System.out.println(moneda1 + " " + moneda2 + " " + moneda3);
		System.out.println();
		
	}

	private static void BorrarTodo() {
		
		Pers.delete();
		
		plat1.delete();
		plat2.delete();
		plat3.delete();
		plat4.delete();
		plat5.delete();
		plat6.delete();
		plat7.delete();
		plat8.delete();
		//plat9.delete();
		//plat10.delete();
		
		Pincho1.delete();
		Pincho2.delete();
		
		MonedaHueca1.delete();
		MonedaHueca2.delete();
		MonedaHueca3.delete();
		
		Moneda1.delete();
		Moneda2.delete();
		//Moneda3.delete();
				
	}
	
	private static void IniciarJuego() {
				
		moneda1 = true;
		moneda2 = true;
		moneda3 = true;
		
		Pers = new pj("Slime", 0, 870, 50, 900, 0, "resources/slime.png", f);
		
		// Plataformas -- Inicio
		
		// Las plataformas miden 175x60
		
		plat1 = new Roca("Plat", 0, 900, 175, 960, 0, "resources/plat.png", f);
		plat2 = new Roca("Plat", 175, 960, 350, 1020, 0, "resources/plat.png", f);
		plat3 = new Roca("Plat", 360, 850, 535, 910, 0, "resources/plat.png", f);
		plat4 = new Roca("Plat", 520, 1000, 695, 1060, 0, "resources/plat.png", f); 
		plat5 = new Roca("Plat", 555, 850, 730, 910, 0, "resources/plat.png", f);
		plat6 = new Roca("Plat", 715, 1000, 890, 1060, 0, "resources/plat.png", f);
		plat7 = new Roca("Plat", 175, 670, 350, 730, 0, "resources/plat.png", f);
		plat8 = new Roca("Plat", 0, 605, 175, 665, 0, "resources/plat.png", f);
		
		// Plataformas -- Final
		
		// Los pinchos miden 90x160
		
		Pincho1 = new Sprite("Pincho", 365, 910, 395, 960, 0, "resources/pincho.png", f);
		Pincho2 = new Sprite("Pincho", 180, 730, 210, 780, 0, "resources/pincho.png", f);
		
		// Bordes
		
		Sprite Muerte = new Sprite("Muerte", 0, 1200, 1600, 1220, 0, "resources/plat.png", f);
		Sprite ParedD = new Sprite("derecha", 1601, 0, 1610, 1200, 0, "resource/plat.png", f);
		Sprite ParedI = new Sprite("izquierda", -10, 0, -1, 1200, 0, "resource/plat.png", f);
		
		// Las monedas miden 45x45
		
		MonedaHueca1 = new Sprite("Hueco", 10, 10, 55, 55, 0, "resources/hueco.png", f);
		MonedaHueca2 = new Sprite("Hueco", 65, 10, 110, 55, 0, "resources/hueco.png", f);
		MonedaHueca3 = new Sprite("Hueco", 120, 10, 165, 55, 0, "resources/hueco.png", f);
		
		// Arriba los huecos de moneda, abajo las monedas del mapa
		
		Moneda1 = new Sprite("coin", 585, 945, 630, 990, 0, "resources/moneda.png", f);
		Moneda2 = new Sprite("coin", 65, 550, 110, 595, 0, "resources/moneda.png", f);
		//Moneda3 = new Sprite("coin", 120, 10, 165, 55, 0, "resources/moneda.png", f);
		
	}

	private static void Monedas() {
		
		if (Pers.monedas == 1 && moneda1) {
			MonedaHueca1.path = "resources/moneda.png";
			moneda1 = false;
		}
		
		if (Pers.monedas == 2 && moneda2) {
			MonedaHueca2.path = "resources/moneda.png";
			moneda2 = false;
		}
		
		if (Pers.monedas == 3 && moneda3) {
			MonedaHueca3.path = "resources/moneda.png";
			moneda3 = false;
		}
		
	}

	private static Set<Character> input() {
		return w.getPressedKeys();
	}

	private static Set<Character> inputd() {
		return w.getKeysDown();
	}
 

}
