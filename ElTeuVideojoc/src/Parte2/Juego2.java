package Parte2;

import Core.Field;
import Core.Window;

public class Juego2 {

	static Field f = new Field();
	
	static Window w = new Window(f);
	
	public static void main(String[] args) throws InterruptedException {
		
		RocaControlable Rokita = new RocaControlable("Roca", 0, 60, 150, 210, 0, "resources/kuentin.jpg", f, 100);
		
		

		boolean salir = false;
				
		while (!salir) {
			f.draw();
			
			Thread.sleep(30);
			
			Input(Rokita);
			if (Rokita.accionesDisponibles == 0) {
				Rokita.delete();
			}
		}
		
	}

	private static void Input(RocaControlable rokita) {
		
		if (w.getPressedKeys().contains('d')) {
			rokita.mov(Input.DERECHA);
		} else if (w.getPressedKeys().contains('w')) {
			rokita.mov(Input.ARRIBA);
		} else if (w.getPressedKeys().contains('s')) {
			rokita.mov(Input.ABAJO);
		} else if (w.getPressedKeys().contains('a')) {
			rokita.mov(Input.IZQUIERDA);
		}
		
	}

}
