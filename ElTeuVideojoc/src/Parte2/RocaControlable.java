package Parte2;

import Core.Field;
import Parte1.Roca;

public class RocaControlable extends Roca{

	public RocaControlable(String string, int i, int j, int k, int l, int m, String string2, Field f, int n) {
		
		super(string, i, j, k, l, m, string2, f, n);
		
	}

	public void mov(Input inp) {
		
		if (inp == Input.DERECHA) {
			this.x1 += 2;
			this.x2 += 2;
			this.accionesDisponibles--;
		} else if (inp == Input.IZQUIERDA) {
			this.x1 -= 2;
			this.x2 -= 2;
			this.accionesDisponibles--;
		} else if (inp == Input.ARRIBA) {
			this.y1 -= 2;
			this.y2 -= 2;
			this.accionesDisponibles--;
		} else if (inp == Input.ABAJO) {
			this.y1 += 2;
			this.y2 += 2;
			this.accionesDisponibles--;
		} 
		
	}

	

}
