package Parte2;

import java.util.Set;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import Parte1.Roca;
import Parte3.Input3;

public class pj extends PhysicBody{

	boolean ColSuelo = false;
	int DobleSalto = 0;
	public int monedas = 0;
	public boolean muerto = false;
	
	public pj(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setConstantForce(0, 0.4);
	}

	public void onCollisionEnter(Sprite sprite) {
		
		if(sprite instanceof Roca) {
			this.ColSuelo = true;
			this.DobleSalto = 0;
		} else {
			this.ColSuelo = false;
		}
		
		if(sprite.name.equals("coin")) {
			sprite.delete();
			this.monedas++;
		}
		
		if(sprite.name.equals("Muerte")) {
			this.delete();
			this.muerto = true;
		}
		
		if(sprite.name.equals("Pincho")) {
			this.delete();
			this.muerto = true;
		}
		
	}

	public void onCollisionExit(Sprite sprite) {
		
	}

	public void mov(Set<Character> teclas) {
		
		if(teclas.contains('d')) {
			
			this.setVelocity(3, this.velocity[1]);
			
		} else

		if (teclas.contains('a')) {
			
			this.setVelocity(-3, this.velocity[1]);
			
		} else {
			
			this.setVelocity(0, this.velocity[1]);
			
		}
			
		
	}
	
	public void Salto() {
	
		if (this.DobleSalto < 2) {
			this.setForce(0, -2.5);
			this.DobleSalto++;
		}
		
	}

	public void Disparar() {
		
		// Hace cosas de disparar
		
	}
	
	/*
	
	public void Reset() {
		
		this.x1 = 0;
		this.x2 = 50;
		this.y1= 870;
		this.y2 = 900;
		
	}
	
	*/

}
