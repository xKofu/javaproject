package Mokepon;

public class MokemonCapt extends Mokemon{

	String apodo;
	
	String trainer;
	
	int felicidad;
	
	Objeto objeto;
	
	public MokemonCapt() {
		
		super();
		
		this.apodo = "Example Nickname";
		
		this.trainer = "Gerard";
		
		this.felicidad = 50;
		
	}
	
	public MokemonCapt(String nombre, Tipos tipo, String apodo) {
		
		super(nombre, tipo);
		
		this.apodo = apodo;
		
		this.trainer = "Gerard";
		
		this.felicidad = 50;
				
	}
	
	public MokemonCapt(String Name, int Nivel, Tipos tipo, String apodo) {
		
		super(Name, Nivel, tipo);
		
		this.apodo = apodo;
		
		this.trainer = "Gerard";
		
		this.felicidad = 50;
		
	}
	
	public MokemonCapt(String Name, int Nivel, int Hp, int atk, int def, int vel, String apodo) {
	
		super(Name, Nivel, Hp, atk, def, vel);
		
		this.apodo = apodo;
		
		this.trainer = "Gerard";
		
		this.felicidad = 50;
		
	}

	public MokemonCapt(Mokemon hey, String mote, String entrenador) {
				
		super(hey.nombre, hey.nivel, hey.hp_max, hey.atk, hey.def, hey.vel);
		
		this.apodo = mote;
		
		this.trainer = entrenador;
		
		this.tipo = hey.tipo;
		
		this.felicidad = 50;
		
		this.ataques = hey.ataques;
		
	}
	
	public void Acariciar() {
		
		if (this.felicidad < 100) {
			this.felicidad += 10;	
		}
		
		if (this.felicidad > 100) {
			this.felicidad = 100;
		}
		
		
	}
	
	public void UsarObjeto(Objeto obj) {
		obj.Usar(this);
	}
	
	@Override
	public String toString() {
		return "MokemonCapt [apodo=" + apodo + ", trainer=" + trainer + ", felicidad=" + felicidad + "]";
	}

	public void Atacar(Mokemon def, int ataque) {
		Ataque a = this.ataques.get(ataque);
		double da�o = ((((((2 * this.nivel)/5)+2) * a.poder * (this.atk / def.def)) / 50) + 2) * super.Efec(a.tipo, def.tipo);
				
		if (this.felicidad < 50) {
			da�o *= 0.8;
		} else {
			da�o *= 1.2;
		}
		
		def.hp_actual = (int) (def.hp_actual - da�o);
		System.out.println("Se ha producido un cate de " + da�o + " de da�o!");
		
	}
	
	public void VerStats() {

		System.out.println("------------");
		System.out.println("Nombre: " + this.nombre);
		System.out.println("Nivel: " + this.nivel);
		System.out.println("Experiencia: " + this.exp);
		System.out.println("Ataque: " + this.atk);
		System.out.println("Defensa: " + this.def);
		System.out.println("Velocidad: " + this.vel);
		System.out.println("Vida Maxima: " + this.hp_max);
		System.out.println("Tipo: " + this.tipo);
		System.out.println("------------");
		System.out.println("Mote: " + this.apodo);
		System.out.println("Entrenador: " + this.trainer);
		System.out.println("Felicidad: " + this.felicidad);
		System.out.println("------------");
	
	}
}
