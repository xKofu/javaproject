package Mokepon;

public class Revivir extends Objeto{

	public Revivir(String name) {
		super(name);
		
	}

	@Override
	public void Usar(Mokemon mon) {
		
		if (mon.debilitado) {
			mon.debilitado = false;
			mon.hp_actual = 1;
			this.Cantidad--;
		}
		
	}

}
