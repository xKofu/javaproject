package Mokepon;

public interface Equipamiento {

	public abstract void Equipar(Mokemon mok);
	
	public abstract void Desequipar(Mokemon mok);
		
	public default boolean PuedeEquipar(Mokemon mok) {
		
		if (mok.ObjEquipado == null && !mok.debilitado) {
			return true;
		} else {
			return false;
		}
	}
	
	public default boolean EquipoMalPuesto(MokemonCapt mok) {
		
		if (mok.objeto instanceof Equipamiento) {
			return true;
		} else {
			return false;
		}
		
	}
	
}
