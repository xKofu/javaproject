package Mokepon;

public class Pocion extends Objeto {

	int hp_curada;
	
	public Pocion(String name) {
		super(name);
		this.hp_curada = 20;
	}

	@Override
	public void Usar(Mokemon mon) {
		
		if (mon.hp_actual == mon.hp_max) {
			mon.hp_actual += this.hp_curada;
			
			if (mon.hp_actual > mon.hp_max) {
				mon.hp_actual = mon.hp_max;
			}
			this.Cantidad--;
		}
		
		
	}

}
