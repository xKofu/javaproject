package Mokepon;

public abstract class Objeto {

	private String nombre;
	
	protected int Cantidad;
	
	public Objeto(String name) {
		
		this.nombre = name;
		this.Cantidad = 1;
		
	}
	
	public abstract void Usar(Mokemon mon);
	
	public void Obtener(int hey) {
		this.Cantidad += hey;
	}
	
	public void Dar(MokemonCapt mon) {
	
		mon.objeto = this;
		
	}
	
	
	
}
