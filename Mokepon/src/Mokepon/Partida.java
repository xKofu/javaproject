package Mokepon;

import java.util.Random;
import java.util.Scanner;

public class Partida {

	public static Random r;
	public static Scanner sc;
	
	public static void main(String[] args) {
		
		r = new Random();
		sc = new Scanner(System.in);
		MokemonCapt PakitoCapt  = null;
		MokemonCapt JoselinCapt = null;
		
		Mokemon Pakito = new Mokemon("Agumon", r.nextInt(100)+1, Tipos.Fuego);
		Mokemon Joselin = new Mokemon("Gabumon", r.nextInt(100)+1, Tipos.Agua);
						
		Ataque mov1a = new Ataque("Llama Bebe", 30, Tipos.Fuego, 20);
		Ataque mov2a = new Ataque("Megallama", 75, Tipos.Fuego, 5);
		
		Ataque mov1g = new Ataque("Fuego Azul", 45, Tipos.Agua, 15);
		Ataque mov2g = new Ataque("Muro de Hielo", 60, Tipos.Agua, 10);
		
		Pakito.A�adirAtaque(mov1a);
		Pakito.A�adirAtaque(mov2a);
		Joselin.A�adirAtaque(mov1g); 
		Joselin.A�adirAtaque(mov2g);
		
		boolean jugar = true;
		
		while (jugar) {
			
			menu();
			int opcion = sc.nextInt();
			
			switch(opcion) {
			case 1:
				if (Debilitado(Pakito, Joselin)) {
					
					System.out.println("Que Mokemon atacar�? ");
					System.out.println("(0) " + Pakito.nombre);
					System.out.println("(1) " + Joselin.nombre);
					int aux = sc.nextInt();
					if(aux == 0) {
						System.out.println("Selecciona un ataque:");
						System.out.println("(0) " + mov1a.nombre );
						System.out.println("(1) " + mov2a.nombre);
						Pakito.Atacar(Joselin, sc.nextInt());
						System.out.println(Joselin.nombre + " tiene ahora " + Joselin.hp_actual + " puntos de vida");

					} else if (aux == 1) {
						System.out.println("Selecciona un ataque:");
						System.out.println("(0) " + mov1g.nombre );
						System.out.println("(1) " + mov2g.nombre);
						Joselin.Atacar(Pakito, sc.nextInt());
						System.out.println(Pakito.nombre + " tiene ahora " + Pakito.hp_actual + " puntos de vida");

					} else {
						System.out.println("Opcion no valida");
					}
					
				} else {
					System.out.println("No se puede luchar si uno de los dos mokemon esta debilitado");
				}
				break;
			case 2:
				Pakito.VerStats();
				Joselin.VerStats();
				break;
			case 3:
				mov1a.Ver();
				mov2a.Ver();
				break;
			case 4:
				mov1g.Ver();
				mov2g.Ver();
				break;
			case 5:
				System.out.println("Se han curado todos los Mokemon!");
				Pakito.Curar();
				Joselin.Curar();
				break;
			case 6:
				PakitoCapt = Pakito.Capturar("Kane", "Gerard");
				JoselinCapt = Capturar(Joselin, "Eric", "Gerard");
				
				PakitoCapt.VerStats();
				JoselinCapt.VerStats();
				break;
			case 7:
				PakitoCapt.Acariciar();
				JoselinCapt.Acariciar();
				break;
			case 0:
				jugar = Cuidao();
				break;
			case 8:
				Pakito = new Mokemon("Agumon", r.nextInt(100), Tipos.Fuego);
				Joselin = new Mokemon("Gabumon", r.nextInt(100), Tipos.Agua);
				
				Pakito.A�adirAtaque(mov1a);
				Pakito.A�adirAtaque(mov2a);
				Joselin.A�adirAtaque(mov1g);
				Joselin.A�adirAtaque(mov2g);
				break;
			default:
				System.out.println("Opcion no valida!");
			
			}
			
		}
		
	}
	
	public static MokemonCapt Capturar(Mokemon capturado, String mote, String trainer) {
		
		if (!(capturado instanceof MokemonCapt)) {
			MokemonCapt Aux = new MokemonCapt(capturado, mote, trainer);
			return Aux;
		} else {
			System.out.println("Manito asi no");
			return (MokemonCapt) capturado;
		}
		
	}
	
	private static boolean Debilitado(Mokemon pakito, Mokemon joselin) {

		if (pakito.hp_actual <= 0) {
			return false;
		} else if (joselin.hp_actual <= 0) {
			return false;
		} else {
			return true;
		}
		
	}
		
	public static void menu() {
		
		System.out.println("---------");
		System.out.println("1. - Luchar");
		System.out.println("2. - Ver Mokemons");
		System.out.println("3. - Ver Ataques 1er Mokemon");
		System.out.println("4. - Ver Ataques 2ndo Mokemon");
		System.out.println("5. - Curar Ambos Mokemon");
		System.out.println("6. - Capturar Mokemon");
		System.out.println("7. - Acariciar Mokemon");
		System.out.println("8. - ReGenerar los bichos");
		System.out.println("0. - Cerrar Programa");
		System.out.println("---------");
		
	}

	private static boolean Cuidao() {

		System.out.println();
		System.out.println("Estas a punto de Salir del Juego, estas seguro? ");
		System.out.println("Pulsa 0 para Si / Pulsa 1 Para No");
		int hey = sc.nextInt();
		if (hey == 0) {
			System.out.println("--> Hasta la Proxima <--");
			return false;
		} else {
			System.out.println("--> El Juego Continua <--");
			return true;
		}

	}
}
