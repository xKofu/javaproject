package Mokepon;

public class Armadura extends Objeto implements Equipamiento {

	int defensaExtra;
	
	public Armadura(String name, int def) {
		super(name);
		this.defensaExtra = def;
	}

	public int getDefensaExtra() {
		return defensaExtra;
	}

	public void setDefensaExtra(int defensaExtra) {
		this.defensaExtra = defensaExtra;
	}

	@Override
	public void Equipar(Mokemon mok) {
		
		if (mok.ObjEquipado == null) {
			mok.ObjEquipado = this;
			mok.def += this.defensaExtra;	
		}
		
	}

	@Override
	public void Desequipar(Mokemon mok) {
		
		if (mok.ObjEquipado == this) {
			mok.ObjEquipado = null;
			mok.def -= this.defensaExtra;
		}
		
	}

	@Override
	public void Usar(Mokemon mon) {
		this.Equipar(mon);
	}

	
	
}
