package Mokepon;

public class Arma extends Objeto implements Equipamiento {

	int ataqueExtra;
	
	public Arma(String name, int atk) {
		super(name);
		this.ataqueExtra = atk;
	}
	

	public int getAtaqueExtra() {
		return ataqueExtra;
	}


	public void setAtaqueExtra(int ataqueExtra) {
		this.ataqueExtra = ataqueExtra;
	}


	@Override
	public void Usar(Mokemon mon) {
		this.Equipar(mon);	
	}

	@Override
	public void Equipar(Mokemon mok) {
		if (mok.ObjEquipado == null) {
			mok.ObjEquipado = this;
			mok.atk += this.ataqueExtra;	
		}
	}

	@Override
	public void Desequipar(Mokemon mok) {
		if (mok.ObjEquipado == this) {
			mok.ObjEquipado = null;
			mok.atk -= this.ataqueExtra;
		}
		
	}

}
