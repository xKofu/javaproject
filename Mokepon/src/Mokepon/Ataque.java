package Mokepon;

public class Ataque {

	String nombre;
	
	double poder;
	
	Tipos tipo;
	
	int mov_max;
	
	int mov_act;

	
	
	@Override
	public String toString() {
		return "Ataque [nombre=" + nombre + ", poder=" + poder + ", tipo=" + tipo + ", mov_max=" + mov_max
				+ ", mov_act=" + mov_act + "]";
	}

	public Ataque(String nombre, double poder, Tipos tipo, int mov_max) {
		
		this.nombre = nombre;
		
		if (poder < 10) {
			this.poder = 10;
		} else if (poder > 100) {
			this.poder = 100;
		} else {
			this.poder = poder;
		}
		
		this.tipo = tipo;
		
		this.mov_max = mov_max;
		
	}
	
	public Ataque(String nombre, Tipos tipo) {
		
		this.nombre = nombre;
		
		this.tipo = tipo;
		
		this.poder = 10;
		
		this.mov_max = 10;
		
	}
	
	public void Ver() {
		
		System.out.println(" <------> ");
		System.out.println("Ataque: " + this.nombre);
		System.out.println("Poder: " + this.poder);
		System.out.println("PP: " + this.mov_max);
		System.out.println("Tipo: " + this.tipo);
		System.out.println(" <------> ");
		
	}
	
}