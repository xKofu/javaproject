package Mokepon;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Testing {

	public static Random r;
	public static Scanner sc;
	
	public static void main(String[] args) {
		
		r = new Random();
		sc = new Scanner(System.in);
		
		Mokemon Pakito = new Mokemon("Agumon", r.nextInt(100), Tipos.Fuego);
		Mokemon Joselin = new Mokemon("Gabumon", r.nextInt(100), Tipos.Agua);
				
		Pakito.VerStats();
		Joselin.VerStats();
		
		
		
		Ataque mov1a = new Ataque("Llama Bebe", 30, Tipos.Fuego, 20);
		Ataque mov2a = new Ataque("Megallama", 75, Tipos.Fuego, 5);
		
		Ataque mov1g = new Ataque("Fuego Azul", 45, Tipos.Agua, 15);
		Ataque mov2g = new Ataque("Muro de Hielo", 60, Tipos.Agua, 10);
		
		Pakito.A�adirAtaque(mov1a);
		Pakito.A�adirAtaque(mov2a);
		Joselin.A�adirAtaque(mov1g);
		Joselin.A�adirAtaque(mov2g);
		
		//mov1a.Ver();
		//mov2a.Ver();
		//mov1g.Ver();
		//mov2g.Ver();
		
		Lucha(Pakito, Joselin, mov1a, mov2a);
				
		MokemonCapt PakitoCapt = Pakito.Capturar("Kane", "Gerard");
		
	}
	
	private static void Lucha(Mokemon pakito, Mokemon joselin, Ataque mov1, Ataque mov2) {
		
		System.out.println("Selecciona un ataque:");
		System.out.println("(0) " + mov1.nombre);
		System.out.println("(1) " + mov2.nombre);
		
		int caso = sc.nextInt();
		double da�o = 0;
		
		switch(caso) {
		case 0:
			
			da�o = ((((((2 * pakito.nivel)/5)+2) * mov1.poder * (pakito.atk / joselin.def)) / 50) + 2) * pakito.Efec(mov1.tipo, joselin.tipo);

			break;
		case 1:
			
			da�o = ((((((2 * pakito.nivel)/5)+2) * mov2.poder * (pakito.atk / joselin.def)) / 50) + 2) * pakito.Efec(mov2.tipo, joselin.tipo);

			break;
		default:
			System.out.println("Fiera no has escogido ningun ataque, vuelve a intentarlo");
		}
		
		joselin.hp_actual = (int) (joselin.hp_max - da�o);		

		System.out.println(joselin.nombre + " ha recibido " + da�o + " puntos de da�o");
		System.out.println(joselin.nombre + " tiene ahora " + joselin.hp_actual + " puntos de vida");

	}

}
