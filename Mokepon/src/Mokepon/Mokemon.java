package Mokepon;

import java.util.ArrayList;
import java.util.Random;

public class Mokemon {

	Random rdom;
	
	String nombre;
	
	int nivel;
	
	int atk;
	
	int def;
	
	int vel;
	
	int exp;
	
	int hp_max;
	
	int hp_actual;
	
	Tipos tipo;
	
	boolean debilitado = false;
	
	ArrayList<Ataque> ataques = new ArrayList<Ataque>(2);
	
	Equipamiento ObjEquipado;
	
	Sexo genero;
	
	@Override
	public String toString() {
		return "Mokemon [rdom=" + rdom + ", nombre=" + nombre + ", nivel=" + nivel + ", atk=" + atk + ", def=" + def
				+ ", vel=" + vel + ", exp=" + exp + ", hp_max=" + hp_max + ", hp_actual=" + hp_actual + ", tipo=" + tipo
				+ ", debilitado=" + debilitado + ", ataques=" + ataques + "]";
	}

	public Mokemon() {
		
		this.nombre = "Example Test";
		this.nivel = 1;
		this.atk = 1;
		this.def = 1;
		this.vel = 1;
		this.hp_max = 10;
		
	}
	
	public Mokemon(String Name, Tipos tipo) {
		
		this.nombre = Name;
		this.nivel = 1;
		this.atk = 1;
		this.def = 1;
		this.vel = 1;
		this.hp_max = 10;
		this.tipo = tipo;
		
	}
	
	public Mokemon(String Name, int Nivel, Tipos tipo) {
		
		rdom = new Random();		
		this.nombre = Name;
		for (int i = 0; i < Nivel; i++) {
			SubirLvl();
		}
		this.tipo = tipo;
		this.hp_actual = this.hp_max;
		int aux = rdom.nextInt(2);		
		if (aux == 0) {
			genero = Sexo.Masculino;
		} else {
			genero = Sexo.Femenino;
		}
	}
	
	public Mokemon(String Name, int Nivel, int Hp, int atk, int def, int vel) {
		
		this.nombre = Name;
		this.nivel = Nivel;
		this.atk = atk;
		this.def = def;
		this.vel = vel;
		this.hp_max = Hp;
		
	}
	
	public void DarXP(int XpOtorgada) {
		
		this.exp += XpOtorgada;
		
		while (this.exp >= 100) {
			
			this.exp -= 100;
			SubirLvl();
			
		}
		
	}

	public void SubirLvl() {
		
		rdom = new Random();
		
		this.nivel++;
		
		this.hp_max += rdom.nextInt(6);
		
		this.atk += rdom.nextInt(3);
		
		this.def += rdom.nextInt(3);
		
		this.vel += rdom.nextInt(3);
		
	}
	
	public void VerStats() {

		System.out.println("------------");
		System.out.println("Nombre: " + this.nombre);
		System.out.println("Nivel: " + this.nivel);
		System.out.println("Experiencia: " + this.exp);
		System.out.println("Ataque: " + this.atk);
		System.out.println("Defensa: " + this.def);
		System.out.println("Velocidad: " + this.vel);
		System.out.println("Vida Maxima: " + this.hp_max);
		System.out.println("Tipo: " + this.tipo);
		System.out.println("------------");
	
	}
	
	public void A�adirAtaque(Ataque at) {
		
		ataques.add(at);
		
	}
	
	public double Efec(Tipos a, Tipos d) {
		
		if (a == Tipos.Fuego && d == Tipos.Planta
		||  a == Tipos.Agua && d == Tipos.Fuego
		||  a == Tipos.Planta && d == Tipos.Agua) {
			return 2;			
		} else if (a == Tipos.Fuego && d == Tipos.Agua
				|| a == Tipos.Agua && d == Tipos.Planta
				|| a == Tipos.Planta && d == Tipos.Fuego) {
			return 0.5;
		} else {
			return 1;
		}
		
	}
	
	public void Atacar(Mokemon def, int ataque) {
		Ataque a = this.ataques.get(ataque);
		double da�o = ((((((2 * this.nivel)/5)+2) * a.poder * (this.atk / def.def)) / 50) + 2) * this.Efec(a.tipo, def.tipo);
		def.hp_actual = (int) (def.hp_actual - da�o);
		System.out.println("Se ha producido un cate de " + da�o + " de da�o!");
	}
	
	public void Curar() {
		
		this.hp_actual = this.hp_max;
		
	}
	
	public MokemonCapt Capturar(String mote, String trainer) {
		
		if (!(this instanceof MokemonCapt)) {
			MokemonCapt Aux = new MokemonCapt(this, mote, trainer);
			return Aux;
		} else {
			System.out.println("Manito asi no");
			return (MokemonCapt) this;
		}
		
	}
	
	public Huevo Reproduccion(Mokemon OtroBicho) throws Exception {
		Huevo Egg;
		rdom = new Random();
		boolean flag1 = false;
		boolean flag2 = false;
		boolean flag3 = false;
		
		if (this.tipo == OtroBicho.tipo) {
			flag1 = true;
		}
		if (this.genero != OtroBicho.genero) {
			flag2 = true;
		}
		if (!this.debilitado && !OtroBicho.debilitado) {
			flag3 = true;
		}
		
		if (flag1 && flag2 && flag3) {
			int aux = rdom.nextInt(2);
			
			if (aux == 1) {
				Egg = new Huevo(this.nombre, this.tipo);
			} else {
				Egg = new Huevo(OtroBicho.nombre, this.tipo);
			}
						
			return Egg;
		} else {
			throw new Exception("Colega los Mokemon no pueden proceder al Coito, una verdadera lastima");
		}
		
	}
	
}


