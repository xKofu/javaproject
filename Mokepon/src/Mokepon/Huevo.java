package Mokepon;

import java.util.Random;

public class Huevo {

	Random rdom;
	
	private String especie;
	
	private Tipos tipo;
	
	private int PasosRestantes;
	
	public Huevo(String especie, Tipos tipo) {
		
		rdom = new Random();
		
		this.especie = especie;
		
		this.tipo = tipo;
		
		this.PasosRestantes = rdom.nextInt(6)+5;
		
	}
	
	public void caminar() {
		
		this.PasosRestantes--;
		
		if (this.PasosRestantes == 0) {
			this.Eclosionar();
		}
		
	}
	
	public Mokemon Eclosionar() {
		
		Mokemon Eclosionado = new Mokemon(this.especie, 5, this.tipo);
		
		return Eclosionado;
		
	}
	
	
}
