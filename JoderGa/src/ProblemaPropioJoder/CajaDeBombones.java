package ProblemaPropioJoder;

import java.util.Scanner;

public class CajaDeBombones {
		
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String[][] mat = new String[5][5];
		
		for (int r = 0; r < mat.length; r++) {
			for (int c = 0; c < mat.length; c++) {
				mat[r][c] = sc.nextLine();
			}
		}
		
		String Letra1 = sc.nextLine();
		String Letra2 = sc.nextLine();
		
		System.out.println(Funcion(mat, Letra1, Letra2));
		
	}
		
	public static int Funcion(String[][] mat, String letra1, String letra2) {
		
		int cont = 0;
		
		for (int r = 0; r < mat.length; r++) {
			for (int c = 0; c < mat.length; c++) {
				if (mat[r][c].equals(letra1) || mat[r][c].equals(letra2)) {
					cont++;
				}
			}
		}
		
		return cont;
	}

}
