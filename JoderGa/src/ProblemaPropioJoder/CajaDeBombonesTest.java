package ProblemaPropioJoder;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CajaDeBombonesTest {

	
	@Test
	void test() {
		String[][] mequieromorir =
		{
				{"A", "E", "B", "C", "A"},
				{"B", "C", "C", "E", "B"},
				{"D", "E", "D", "A", "B"},
				{"E", "C", "B", "A", "A"},
				{"A", "B", "E", "E", "D"}
		};
		assertEquals(12, CajaDeBombones.Funcion(mequieromorir, "A", "E"));
		
		String[][] mequieromorir2 =
		{
				{"B", "E", "A", "D", "A"},
				{"E", "B", "A", "E", "E"},
				{"C", "C", "E", "C", "D"},
				{"C", "B", "A", "B", "B"},
				{"A", "B", "E", "E", "D"}
		};
		assertEquals(9, CajaDeBombones.Funcion(mequieromorir2, "B", "D"));

	}

}


