package CodejamJoder;

import java.util.ArrayList;
import java.util.Scanner;

public class DelPozoALaLuna {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int casos =  sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int palabros = sc.nextInt();
			sc.nextLine();
			ArrayList<String> lista = new ArrayList<>();
			
			for (int j = 0; j < palabros; j++) {
				lista.add(sc.nextLine());
			}
			
			int cont = 0;
			int correctos = 0;
			char letra = ' ';
			char letra2 = ' ';
			String estoytriste = lista.get(0);
			
			for (int j = 1; j < palabros; j++) { 
				
				for (int j2 = 0; j2 < estoytriste.length(); j2++) {
					letra = lista.get(j).charAt(j2);
					letra2 = lista.get(j-1).charAt(j2);
					
					if (letra == letra2) {
						cont++;
					}
				}
								
				if (cont == estoytriste.length()-1) {
					correctos++;
				} 
				cont = 0;
			}
			
			if (correctos == palabros-1) {
				System.out.println("CORRECTE");
			} else {
				System.out.println("INCORRECTE");
			}
			correctos = 0;
		}
		
	}

}
