package CodejamJoder;

import java.util.ArrayList;
import java.util.Scanner;

public class PremiInesperat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);

		int nums = sc.nextInt();

		while (nums != 0) {

			ArrayList<Integer> lista = new ArrayList<>();

			while (nums != 0) {
				lista.add(nums);
				nums = sc.nextInt();
			}

			int numc = sc.nextInt();
			int numj = sc.nextInt();

			if (lista.indexOf(numc) < lista.indexOf(numj)) {
				System.out.println("CARLOTA");
			} else {
				System.out.println("JOANA");
			}

			nums = sc.nextInt();
		}
	}

}
