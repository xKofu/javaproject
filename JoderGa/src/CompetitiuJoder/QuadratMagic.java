package CompetitiuJoder;

import java.util.Scanner;

public class QuadratMagic {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		int[][] mat = new int[3][3];
		boolean flag  = false;
		boolean pri = false;
		boolean seg = false;
		boolean ter = false;
		boolean cua = false;
		boolean qui = false;
		boolean sex = false;
		boolean sep = false;
		boolean oct = false;
		boolean nov = false;
		
		for (int i = 0; i < casos; i++) {
			
			for (int r = 0; r < 3; r++) {
				for (int c = 0; c < 3; c++) {
					mat[r][c] = sc.nextInt();
					
				}
			}
			int cont = 0;
			for (int r = 0; r < 3; r++) {
				for (int c = 0; c < 3; c++) {
					if (mat[r][c] == 1 && !pri) {
						cont++;
						pri = true;
					}
					if (mat[r][c] == 2 && !seg) {
						cont++;
						seg = true; 
					}
					if (mat[r][c] == 3 && !ter) {
						cont++;
						ter = true;
					}
					if (mat[r][c] == 4 && !cua) {
						cont++;
						cua = true;
					}
					if (mat[r][c] == 5 && !qui) {
						cont++;
						qui = true;
					}
					if (mat[r][c] == 6 && !sex) {
						cont++;
						sex = true;
					}
					if (mat[r][c] == 7 && !sep) {
						cont++;
						sep = true;
					}
					if (mat[r][c] == 8 && !oct) {
						cont++;
						oct = true;
					}
					if (mat[r][c] == 9 && !nov) {
						cont++;
						nov = true;
					}
					
					
				}
			}
			if (cont != 9) {
				flag = true;
			}
			
			int aux  = 0;
			for (int r = 0; r < 3; r++) {
				for (int c = 0; c < 3; c++) {
					aux += mat[r][c];
				}
				if (aux != 15) {
					flag = true;
				}
				aux = 0;
			}
			for (int r = 0; r < 3; r++) {
				for (int c = 0; c < 3; c++) {
					aux += mat[c][r];
				}
				if (aux != 15) {
					flag = true;
				}
				aux = 0;
			}
			
			for (int j = 0; j < 3; j++) {
				aux += mat[j][j];
			}
			if (aux != 15) {
				flag = true;
			}
			aux = 0;
			int hey = 2;
			for (int j = 0; j < 3; j++) {
				aux += mat[j][hey];
				hey--;
			}
			if (aux != 15) {
				flag = true;
			}
			aux = 0;
			
			if (flag) {
				System.out.println("NO");
				flag = false;
			} else {
				System.out.println("SI");
			}
		}
		
	}

}
