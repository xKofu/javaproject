package CompetitiuJoder;

import java.util.Scanner;

public class PiramideDeCartes {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int hey = sc.nextInt();
			int buenas = 1;
			
			while (hey > 0) {
				
				if (hey - (2 * buenas + buenas - 1) >= 0) {
					
					hey -= (2 * buenas + buenas - 1);
					buenas++;
					
				} else {
					break;
				}
				
			}
			
			System.out.println(buenas-1 + " " + hey);
			
		}
		
	}
		
}


