package CompetitiuJoder;

import java.util.Scanner;

public class TaulellEscacs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		int dimension1 = sc.nextInt();
		int dimension2 = sc.nextInt();
		String[][] hey = new String[dimension1][dimension2];
		int cont = 0;
		System.out.println();System.out.println();
		
		for (int r = 0; r < dimension1; r++) {
			for (int c = 0; c < dimension2; c++) {
				if (cont%2 == 0) {
					hey[r][c]=". ";
					cont++;
				}
				else {
					hey[r][c]="# ";
					cont++;
				}
			}
			cont++;	
		}
		
		for (int r = 0; r < dimension1; r++) {
			for (int c = 0; c < dimension2; c++) {
				System.out.print(hey[r][c]);
			}
			System.out.println();
		}
	}

}
