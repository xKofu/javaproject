package Codejam;

import java.util.Scanner;

public class SimplificaMe {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int num1 = sc.nextInt();
			
			int num2 = sc.nextInt();
			
			int num1a = num1/10;
			
			int num1b = num1%10;
			
			int num2a = num2/10;
			
			int num2b = num2%10;
				
			double res1 = (double) num1 / (double) num2;
			
			double res2 = -1;
			
			if (num1a == num2a) {
				
				// 1 1
				
				res2 = (double) num1b/ (double) num2b;
												
			} else if (num1a == num2b) {
				
				// 1 2
				
				res2 = (double) num1b/ (double) num2a;
												
			} else if (num1b == num2a) {
				
				// 2 1
				
				res2 = (double) num1a/ (double) num2b;
							
			} else if (num1b == num2b) {
				
				// 2 2
				
				res2 = (double) num1a/ (double) num2a;
				
			}
								
			
			if (res1 == res2) {
				System.out.println("SI");
			} else {
				System.out.println("NO");
			}
		}
		
	}

}
