package Codejam;

import java.util.Scanner;

public class MickyMouse {

	public static void main(String[] args) {
		
		Scanner xd = new Scanner(System.in);
		
		int casos = xd.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int num = xd.nextInt();
			
			boolean flag = false;
			int cont = 0;
			
			System.out.println("NUMERO " + num);
			
			while(num > 1) {
				
				
				if (num%2 == 1) {
					if (!flag) {
						num++;
					}
					flag = true;
					
				}
				cont++;
				num = num/2;
				System.out.print(num + " ");
				
			}
			System.out.println();
			System.out.println("CONTADOR " + cont);
			
		}
		
	}

}
