package IntroduccioJoder;

import java.util.Scanner;

public class Bucles1_ValorMesGranYMesPetit {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int hey = sc.nextInt();
		int un = hey;
		int lol = hey;
		
		while (hey != 0) {
						
			if (hey > un) {
				un = hey;
			}
			if (hey < lol) {
				lol = hey;
			}
			
			hey = sc.nextInt();
		}
		
		System.out.println(un + " " + lol);

	}

}
