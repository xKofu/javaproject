package IntroduccioJoder;
import java.util.Scanner;

public class Bucles1_Nota10 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int x = sc.nextInt();
		int cont10 = 0;
		int contn = 0;
		
		while (x != -1) {
			if (x == 10) {
				cont10++;
				contn++;
			} else if (x < 10 && x >= 0) {
				contn++;
			}
			x = sc.nextInt();
		}
		
		System.out.println("TOTAL NOTES: " + contn + " NOTES10: " + cont10);
	}

}
