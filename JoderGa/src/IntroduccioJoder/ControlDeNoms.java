package IntroduccioJoder;

import java.util.Scanner;

public class ControlDeNoms {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String entrada = sc.nextLine();
		boolean flag = false;
		for (int i = 1; i < entrada.length(); i++) {
			if (entrada.charAt(i) != entrada.charAt(i-1)) {
				flag = true;
				break;
			} 
		}
		if (flag) {
			System.out.println("SI");
		} else {
			System.out.println("NO");
		}
	}

}
