package IntroduccioJoder;

import java.util.ArrayList;
import java.util.Scanner;

public class EscriuEnUnArray3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		ArrayList<Integer> lista = new ArrayList<>();
		
		do {
			lista.add(num);
			num = sc.nextInt();
		} while (num != -1);
		
		int pos = sc.nextInt();
		
		System.out.println(lista);
		System.out.println(lista.get(pos));
	}

}
