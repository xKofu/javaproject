package IntroduccioJoder;

import java.util.Scanner;

public class ContarLA {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		sc.nextLine();
		int cont = 0;
		
		for (int i = 0; i < casos; i++) {
			
			String frase = sc.nextLine().toLowerCase();
			for (int j = 1; j < frase.length(); j++) {
				char succ = frase.charAt(j-1);
				char succ2 = frase.charAt(j);
				if (succ == 'l' && succ2 == 'a') {
					cont++;
				}
			}
			
			System.out.println(cont);
			cont = 0;
		}
	}

}
