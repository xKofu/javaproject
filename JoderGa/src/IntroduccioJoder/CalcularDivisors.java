package IntroduccioJoder;

import java.util.Scanner;

public class CalcularDivisors {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			
			int num = sc.nextInt();
			for (int j = 0; j <= num; j++) {
				if (j != 0) {
					if (num%j == 0) {
						System.out.print(j + " ");
					}
				}
				
			}
			System.out.println();
		
		}
	}

}
