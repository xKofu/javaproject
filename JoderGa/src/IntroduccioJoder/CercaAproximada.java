package IntroduccioJoder;

import java.util.Scanner;

public class CercaAproximada {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		int vueltas = sc.nextInt();
		int arraynum;
		boolean flag = false;
		
		for (int i = 0; i < vueltas; i++) {
			arraynum = sc.nextInt();
			int[] sapatos = new int[arraynum];
			for (int j = 0; j < arraynum; j++) {
				sapatos[j] = sc.nextInt();
			}
			
			int numsapato = sc.nextInt();
			
			for (int j = 0; j < arraynum ; j++) {
				if ( (numsapato + 1) == sapatos[j] 
				||   (numsapato - 1) == sapatos[j] ) {
					flag = true;
				}
			}
			if (flag) {
				System.out.println("SI");
				flag = false;
			} else {
				System.out.println("NO");
			}
		}
	}

}
