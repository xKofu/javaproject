package IntroduccioJoder;

import java.util.Scanner;

public class EscriuEnUnArray1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int hey = sc.nextInt();
		
		int[] array = new int[hey];
		
		for (int i = 0; i < array.length; i++) {
			array[i] = sc.nextInt();
		}
		
		int pos = sc.nextInt();
		
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
		
		System.out.println(array[pos]);
	}

}
