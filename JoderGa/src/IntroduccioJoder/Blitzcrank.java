package IntroduccioJoder;

import java.util.Scanner;

public class Blitzcrank {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		Scanner sc = new Scanner(System.in);
		
		int a,b;
		double c;
		
		do {
		    
		a = sc.nextInt();
		b = sc.nextInt();
		c = sc.nextDouble();
		
		if (a == 0 && b == 0 && c == 0) {
		    break;
		}
		
		int aux1 = (a*a)+(b*b);
		
		double hipotenusa =  Math.sqrt(aux1);
				
		if (c >= hipotenusa) {
			System.out.println("SI");
		} else {
			System.out.println("NO");
		}
		    
		} while(a != 0 && b != 0 && c != 0);
		
	}

}
