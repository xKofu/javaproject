package IntroduccioJoder;

import java.util.Scanner;

public class EscriuEnUnArray2 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int arraynum =  sc.nextInt();
		
		String[] arrayS = new String[arraynum+1];
				
		for (int i = 0; i < arrayS.length; i++) {
			arrayS[i] = sc.nextLine();
		}
		
		int pos = sc.nextInt();
		
		System.out.println(arrayS[pos+1]);
	}

}
