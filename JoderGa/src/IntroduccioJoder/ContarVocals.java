package IntroduccioJoder;

import java.util.Scanner;

public class ContarVocals {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		int pares = 0;
		int nones = 0;
		
		for (int i = 0; i < casos; i++) {
						
			int num = sc.nextInt();
			
			if (num%2 == 0) {
				pares += num;
			} else {
				nones += num;
			}
			
		}
		
		System.out.println("PARELLS: " + pares + " SENARS: " + nones);
					

	}

}
