package IntroduccioJoder;
import java.util.Scanner;

public class Bucles1_SumaPositiusNegatius {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		int x = sc.nextInt();
		int positivo = 0;
		int negativo = 0;
		

		while ( x != 0) {
			if (x > 0) {
				positivo++;
			} else {
				negativo++;
			}
			x = sc.nextInt();
		}
		
		if (positivo > negativo) {
			System.out.println("POSITIUS");
		} else if (negativo > positivo) {
			System.out.println("NEGATIUS");
		} else {
			System.out.println("IGUALS");
		}

	}

}
