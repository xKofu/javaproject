package ExternJoder;

import java.util.Scanner;

public class HoraDeDescomprimir {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			 
			int vueltas = sc.nextInt();
			String simbolo = sc.next();
			
			for (int j = 0; j < vueltas; j++) {
				System.out.print(simbolo);
			}
			System.out.println();
		}
	}
}
