package CodigosEduard;

import java.util.Random;
import java.util.Scanner;

// @author GERARD LASTRES

public class DadosEduard {

	public static void main(String[] args) {		

		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		
		
		System.out.println("Numero Jugador 1: ");
		int num1 = sc.nextInt();
		System.out.println("Apuesta Jugador 1: ");
		int apuesta1 = sc.nextInt();
		System.out.println("Numero Jugador 2: ");
		int num2 = sc.nextInt();
		System.out.println("Apuesta Jugador 2: ");
		int apuesta2 = sc.nextInt();
		
		int dado1 = r.nextInt(6)+1;
		int dado2 = r.nextInt(6)+1;
		int dado3 = r.nextInt(6)+1;
		
		int resultado = dado1 + dado2 + dado3;
		
		String lmao;
		int cositas;
		
		if (num1 == resultado && num2 == resultado) {
			lmao = "AMBOS HAN GANADO!";
			cositas = 0;
		} else if (num1 == resultado) {
			lmao = "HA GANADO EL JUGADOR 1!";
			cositas = apuesta1 + apuesta2;
		} else
		if (num2 == resultado) {
			lmao = "HA GANADO EL JUGADOR 2";
			cositas = apuesta2 + apuesta1;
		} else {
			lmao = "HA GANADO LA BANCA";
			cositas = apuesta1 + apuesta2;
		}
		
		System.out.println("Resultado del Dado 1: " + dado1);
		System.out.println("Resultado del Dado 2: " + dado2);
		System.out.println("Resultado del Dado 3: " + dado3);
		System.out.println("_________________________");
		System.out.println("Resultado de la suma: " + resultado);
		System.out.println("Por lo tanto...");
		System.out.println(lmao);
		System.out.println("_________________________");
		System.out.println("Valor Final Ganado " + cositas + "�");
		
		}

}
