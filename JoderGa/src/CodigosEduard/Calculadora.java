package CodigosEduard;

import java.util.Scanner;

public class Calculadora {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Primero Numero a calcular:");
		double num1 = sc.nextDouble();
		System.out.println("Que operacion quieres realizar?");
		String op = sc.next().toLowerCase();
		System.out.println("Segundo Numero a calcular");
		double num2 = sc.nextDouble();

		double resultat;
				
		switch(op) {
		case "+":
		case "suma":
			resultat = num1 + num2;
			System.out.println("El resultado de la operación es: " + resultat);
			break;
		case "-":
		case "resta":
			resultat = num1 - num2;
			System.out.println("El resultado de la operación es: " + resultat);
			break;
		case "*":
		case "multiplicar":
			resultat = num1 * num2;
			System.out.println("El resultado de la operación es: " + resultat);
			break;
		case "/":
		case "dividir":
			resultat = num1 / num2;
			System.out.println("El resultado de la operación es: " + resultat);
			break;
		default:
			System.out.println("No has realizado una operación valida");
		}
	}
}
