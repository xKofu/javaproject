package CodigosEduard;

import java.util.Scanner;

public class Numero_caracteres {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		String hey1 = sc.nextLine();
		
		String hey1spaceless = hey1.replaceAll(" ", ""); 	// Cambiamos los espacios por la nada
		int hey1contador = hey1spaceless.length(); 			// Contamos los caracteres
		
		System.out.println("Sin espacios: " + hey1contador);
		System.out.println("Con espacios: " + hey1.length());
		
		
	}

}