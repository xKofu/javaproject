package ExamenMatrius;

import java.util.Random;
import java.util.Scanner;

public class ElTesorilloEj3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		Random rdom = new Random();
		
		System.out.println("Filas que tendra el jardin: ");
		int row = sc.nextInt();
		System.out.println("Columnas que tendra el jardin: ");
		int col = sc.nextInt();
		
		String[][] jardin = new String[row][col];
		
		for (int r = 0; r < row; r++) {
			for (int c = 0; c < col; c++) {
				jardin[r][c] = ". ";
			}
		}
		
		jardin[rdom.nextInt(row)][rdom.nextInt(col)] = "T ";
		
		System.out.println("Cuandos intentos quires hacer?: ");
		int n = sc.nextInt();
		
		for (int i = 0; i < n; i++) {
			
			int intento = i + 1;
			System.out.println("Este es el intento numero: " + intento);
			
			System.out.println("Fila Candidata: ");
			int fc = sc.nextInt();
			
			System.out.println("Columna Candidata: ");
			int cc = sc.nextInt();
			
			if (jardin[fc][cc].equals("T ")) {
				System.out.println("Has encontrado el Tesoro!");
				break;
			} else {
				System.out.println("No has acertado, mas suerte a la proxima!");
				jardin[fc][cc] = "E ";
			}
			
		}
		
		System.out.println("Este es el jardin: ");
		System.out.println();
		for (int r = 0; r < row; r++) {
			for (int c = 0; c < col; c++) {
				System.out.print(jardin[r][c]);
			}
			System.out.println();
		}
	}

}
