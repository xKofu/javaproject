package ExamenMatrius;

import java.util.Scanner;

public class EscacsGemelosFacunEj1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		int peones = sc.nextInt();
		
		int[][] tablero = new int[8][8];
		
		for (int i = 0; i < peones; i++) {
			
			tablero[sc.nextInt()][sc.nextInt()] = 1;
			
		}
		
		int cont = 0;
		int f = 0;
		
		for (int c = 0; c < tablero.length; c++) {
			for (int r = 0; r < tablero.length; r++) {
				if (tablero[r][c] == 1) {
					cont++;
				}
			}
			
			if (cont > 1) {
				f++;
			}
			cont = 0;
		}
		
		System.out.println(f);
		
		for (int r = 0; r < tablero.length; r++) {
			for (int c = 0; c < tablero.length; c++) {
				System.out.print(tablero[r][c] + " ");
			}
			System.out.println();
		}
		
	}

}
