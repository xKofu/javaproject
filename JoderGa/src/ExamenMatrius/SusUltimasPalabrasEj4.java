package ExamenMatrius;

import java.util.ArrayList;
import java.util.Scanner;

public class SusUltimasPalabrasEj4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		String palabra = sc.nextLine();
		
		ArrayList<String> lista = new ArrayList<>();
		
		while (!palabra.equals("FI")) {
			
			lista.add(palabra);
			
			palabra = sc.nextLine();
			
		}
		
		if (lista.size() > 5) {
			
			for (int i = lista.size()-5; i < lista.size(); i++) {
				System.out.println(lista.get(i));
			}
		
		} else {
			
			for (int i = 0; i < lista.size(); i++) {
				System.out.println(lista.get(i));
			}		
		
		}
		
		
	}

}
