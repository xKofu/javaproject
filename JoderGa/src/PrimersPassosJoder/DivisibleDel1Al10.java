package PrimersPassosJoder;

import java.util.Scanner;

public class DivisibleDel1Al10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		
		if ((num%2) == 0 && (num%3) == 0 && (num%5) == 0 && (num%7) == 0) {
			System.out.println("SI");
		} else {
			System.out.println("NO");
		}
	}
}
