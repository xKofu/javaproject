package PrimersPassosJoder;

import java.util.Scanner;

public class ButlletiNotes {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int nota = sc.nextInt();
		
		if (nota <= 4) {
			System.out.println("Suspes");
		}else
		if (nota <= 6) {
			System.out.println("Aprovat");
		}else
		if (nota <= 8) {
			System.out.println("Notable");
		}else {
			System.out.println("Excelent");
		}
		
	}

}
