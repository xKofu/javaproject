package PrimersPassosJoder;

import java.util.Scanner;

public class ParesYNones {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int vueltas = sc.nextInt();
		String haha = sc.nextLine();
		int pares = 0;
		int nones = 0;
		char num = 0;
		
		for (int i = 0; i < vueltas; i++) {
			haha = sc.nextLine();
			for (int j = 0; j < haha.length(); j++) {
				if (j % 2 == 0) {
					num = haha.charAt(j);
					pares += num - 48;
					
				} else {
					num = haha.charAt(j);
					nones += num - 48;
				}
			}
			System.out.println(pares + " " + nones);
			pares = 0;
			nones = 0;
		}
		
	}

}
