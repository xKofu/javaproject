package PrimersPassosJoder;

import java.util.Scanner;

public class Blitzcrank {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		Scanner sc = new Scanner(System.in);
		
		int a = sc.nextInt();
		int b = sc.nextInt();
		int c = sc.nextInt();
		
		int aux1 = (a*a)+(b*b);
		
		double hipotenusa =  Math.sqrt(aux1);
		
		if (c <= hipotenusa) {
			System.out.println("SI");
		} else {
			System.out.println("NO");
		}
		
	}

}
