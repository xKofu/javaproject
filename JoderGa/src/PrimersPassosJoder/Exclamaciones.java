package PrimersPassosJoder;

import java.util.Scanner;

public class Exclamaciones {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		String palabro = sc.nextLine();
		int cont1 = 0;
		int cont2 = 0;

		while (!palabro.equals("FIN")) {
			for (int i = 0; i < palabro.length(); i++) {
				char letra = palabro.charAt(i);
				if (letra == '!') {
					cont1++;
				}
				if (letra == '�') {
					cont2++;
				}
			}

			if (cont1 == cont2) {
				System.out.println("SI");
			} else {
				System.out.println("NO");
			}
			cont1 = 0;
			cont2 = 0;
			palabro = sc.nextLine();
		}
	}

}
