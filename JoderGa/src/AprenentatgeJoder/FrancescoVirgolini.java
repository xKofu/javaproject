package AprenentatgeJoder;

import java.util.ArrayList;
import java.util.Scanner;

public class FrancescoVirgolini {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();

		for (int i = 0; i < casos; i++) {
			
			int vueltas = sc.nextInt();
			sc.nextLine();
			ArrayList<String> lista = new ArrayList<>();
			
			for (int j = 0; j < vueltas; j++) {
				lista.add(sc.nextLine());
			}
			
			int indice = lista.indexOf("Francesco Virgolini");
			
			String aux = lista.get(indice - 1);
			lista.set(indice - 1, "Francesco Virgolini");
			lista.set(indice, aux);

			System.out.println(lista);
		}

	}

}
