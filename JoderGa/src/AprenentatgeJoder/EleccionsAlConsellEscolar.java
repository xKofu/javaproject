package AprenentatgeJoder;

import java.util.Scanner;

public class EleccionsAlConsellEscolar {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int fila = sc.nextInt();
		
		String[][] mat = new String[fila][fila];
		
		for (int r = 0; r < fila; r++) {
			int hey = fila-1;
			for (int c = 0; c < fila; c++) {
				if (r==0 || r==fila-1 || c==0 || c==fila-1) {
					mat[r][c] = "X";
				} else if (r == c) {
					mat[r][c] = "X";
				}else if (r == hey) {
					mat[r][c] = "X";
				}else {
					mat[r][c] = ".";
				}
				hey--;
			}
		}
		
		for (int r = 0; r < fila; r++) {
			for (int c = 0; c < fila; c++) {
				System.out.print(mat[r][c]);
			}
			System.out.println();
		}
		
	}

}
