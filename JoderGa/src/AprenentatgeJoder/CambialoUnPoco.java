package AprenentatgeJoder;

import java.util.Scanner;

public class CambialoUnPoco {

	public static void main(String[] args) {
			
		Scanner sc = new Scanner(System.in);
		
		int rondas = sc.nextInt();
		
		for (int i = 0; i < rondas; i++) {
			
			int rondas2 = sc.nextInt();
			int[] array = new int[rondas2];
			
			for (int j = 0; j < rondas2; j++) {
				array[j] = sc.nextInt();
			}
			
			int acambiar = sc.nextInt();
			int cambio = sc.nextInt();
			
			for (int j = 0; j < array.length; j++) {
				if (array[j] == acambiar) {
					array[j] = cambio;
				}
			}
			
			for (int j = 0; j < array.length; j++) {
				System.out.print(array[j] + " ");
			}
			
			System.out.println();
		}
		
	}

}
