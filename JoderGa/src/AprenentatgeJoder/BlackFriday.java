package AprenentatgeJoder;

import java.util.Scanner;

public class BlackFriday {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int fila = sc.nextInt();
		int columna = sc.nextInt();
		
		int[][] mat = new int[fila][columna];
		
		for (int r = 0; r < fila; r++) {
			for (int c = 0; c < columna; c++) {
				mat[r][c] = sc.nextInt();
			}
		}
		
		int op = sc.nextInt();
		
		for (int r = 0; r < fila; r++) {
			for (int c = 0; c < columna; c++) {
				mat[r][c] *= op;
			}
		}
		
		for (int r = 0; r < fila; r++) {
			for (int c = 0; c < columna; c++) {
				System.out.print(mat[r][c] + " ");
			}
			System.out.println();
		}
	}

}
