package AprenentatgeJoder;

import java.util.Scanner;

public class Murcielago {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		String palabro = sc.nextLine();
		boolean a = false;
		boolean e = false;
		boolean hi = false;
		boolean o = false;
		boolean u = false;
		
		for (int i = 0; i < palabro.length(); i++) {
			
			char letra = palabro.charAt(i);
			if ( letra == 'a') {
				a = true;
			}
			if ( letra == 'e') {
				e = true;
			}
			if ( letra == 'i') {
				hi = true;
			}
			if ( letra == 'o') {
				o = true;
			}
			if ( letra == 'u') {
				u = true;
			}
		}
		
		if (a && e && hi && o && u) {
			System.out.println("TOTES");
		} else {
			System.out.println("FALTEN");
		}
	}

}
