package AprenentatgeJoder;

import java.util.Scanner;
import java.util.TreeSet;

public class GuarderiaPokemon {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		String caso = sc.nextLine();
		
		TreeSet<String> lista = new TreeSet<String>();
		
		while (!caso.equals("D")) {
			if (caso.equals("A")) {
				lista.add(sc.nextLine());
			} else
			if (caso.equals("B")) {
				lista.remove(sc.nextLine());
			} else
			if (caso.equals("C")) {
				lista.clear();
			}
			caso = sc.nextLine();
		}
		System.out.println(lista);
	}

}
