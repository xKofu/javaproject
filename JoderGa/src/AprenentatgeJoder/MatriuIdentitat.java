package AprenentatgeJoder;

import java.util.Scanner;

public class MatriuIdentitat {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int hey = sc.nextInt();
		int[][] mat = new int[hey][hey];
		
		for (int i = 0; i < hey; i++) {
			for (int j = 0; j < hey; j++) {
				if (j == i) {
					mat[i][j] = 1;
				} else {
					mat[i][j] = 0;
				}
			}
		}
		for (int i = 0; i < hey; i++) {
			for (int j = 0; j < hey; j++) {
				System.out.print(mat[i][j] + " ");
			}
			System.out.println();
		}
		
	}

}
