package AprenentatgeJoder;

import java.util.Scanner;

public class MatriuDeMaxim {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int row = sc.nextInt();
			int col = sc.nextInt();
			int[][] mat = new int[row][col];
			
			int aux = 0;
			int rowaux = 0;
			int colaux = 0;
			
			for (int r = 0; r < row; r++) {
				for (int c = 0; c < col; c++) {
					mat[r][c] = sc.nextInt();
					if (mat[r][c] > aux) {
						aux = mat[r][c];
						rowaux = r+1;
						colaux = c+1;
					}
				}
			}
			System.out.println(rowaux + " " + colaux);
		}
	}

}
