package AprenentatgeJoder;

import java.util.Scanner;

public class SumaDePatates {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		int suma = 0;
		
		for (int i = 0; i < casos; i++) {
			
			int camiones = sc.nextInt();
			
			for (int j = 0; j < camiones; j++) {
				suma += sc.nextInt();
			}
			
			System.out.println(suma);
			suma = 0;
		}

	}

}
