package AprenentatgeJoder;

import java.util.HashSet;
import java.util.Scanner;

public class Unique {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int vueltas = sc.nextInt();
			HashSet<String> lista = new HashSet<>();
			sc.nextLine();
			
			for (int j = 0; j < vueltas; j++) {
				lista.add(sc.nextLine());
			}
			
			System.out.println(lista);
		}
	}

}
