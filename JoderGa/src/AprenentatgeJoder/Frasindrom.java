package AprenentatgeJoder;

import java.util.Arrays;
import java.util.Scanner;

public class Frasindrom {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		String caso = sc.nextLine();
		
		while (!caso.equals(".")) {
			
		String[] array1 = caso.split(" ");
		String[] array2 = new String[array1.length];
	
		int j = 0;
		for (int i = array1.length-1; i >= 0; i--) {
			array2[j] = array1[i];
			j++;
		}
				
		if (Arrays.equals(array1, array2)) {
			System.out.println("SI");
		} else {
			System.out.println("NO");
		}
		caso = sc.nextLine();
		}
	}

}
