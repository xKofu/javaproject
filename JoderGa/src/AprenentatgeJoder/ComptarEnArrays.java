package AprenentatgeJoder;

import java.util.Scanner;

public class ComptarEnArrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		int rondas = sc.nextInt();
				
		for (int i = 0; i < rondas; i++) {
			
			int numarray = sc.nextInt();
			int[] array = new int[numarray];
			
			for (int j = 0; j < numarray; j++) {
				array[j] = sc.nextInt();
			}
			
			int numcon = sc.nextInt();
			int cont = 0;
			
			for (int j = 0; j < numarray; j++) {
				if (array[j] == numcon) {
					cont++;
				}
			}
			System.out.println(cont);
		}
	}

}
