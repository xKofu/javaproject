package AprenentatgeJoder;

import java.util.LinkedHashMap;
import java.util.Scanner;

public class ElPrimerQueArribia5 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String hey = sc.nextLine();
		
		LinkedHashMap<String,Integer> buenas = new LinkedHashMap<String,Integer>();
		
		while (!hey.equals("xxx")) {
			
			if (buenas.containsKey(hey)) {
				int pspsps = buenas.getOrDefault(hey, 0);
				pspsps++;
				buenas.replace(hey, pspsps);
			} else {
				buenas.put(hey, 1);
			}
			
			hey = sc.nextLine();
		}
		
		String fin = null;
		boolean flag = false;
		
		for (String nom : buenas.keySet()) {
			if (buenas.getOrDefault(nom, 0) >= 5) {
				flag = true;
				fin = nom;
				break;
			}
		}
		
		if (flag) {
			System.out.println(fin);
		} else {
			System.out.println("NO");
		}
		
	}

}
