package AprenentatgeJoder;

import java.util.Scanner;

public class ParaulaMesLlarga {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int casos =  sc.nextInt();
		sc.nextLine();
		int valor1 = 0;
		int valor2 = 0;
		
		for (int i = 0; i < casos; i++) {
			
			String frase = sc.nextLine();
			String[] array = frase.split(" ");
			
			for (int j = 0; j < array.length; j++) {
				
				valor1 = array[j].length();
				
				if (valor1 > valor2) {
					valor2 = valor1;
				}
			}
			
			System.out.println(valor2);
			valor1 = 0;
			valor2 = 0;
		}
	}

}
