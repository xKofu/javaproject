package AprenentatgeJoder;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class CercaPerValorDiccionari {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int vueltas = sc.nextInt();
			sc.nextLine();
			HashMap<String,String> lista = new HashMap<String,String>();
			
			
			for (int j = 0; j < vueltas-1; j++) {
				String hey = sc.nextLine();
				String[] buenas = hey.split("-");
				lista.put(buenas[0], buenas[1]);
			}
			
			String comp = sc.nextLine();
			String fin = null;
			
			for (String nom : lista.keySet()) {
				if (lista.get(nom).equals(comp)) {
					fin = nom;
					break;
				}
			}
			
			System.out.println(lista);
			System.out.println(fin);
			
		}
		
		
		
	}

}
