package CodigosGregorio;

import java.util.Random;
import java.util.Scanner;

public class ExtraExamen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		Random rdom = new Random();
		
		int opcion = 0;
		
		int[][] mat = new int[8][8];
		
		for (int r = 0; r < mat.length; r++) {
			for (int c = 0; c < mat.length; c++) {
				mat[r][c] = 0;
			} 
		}
				
		do {
			
			System.out.println("Escoge una de las siguientes opciones:");
			System.out.println("1) Peones a colocar");
			System.out.println("2) Mostrar la posicion de los peones");
			System.out.println("3) Mostrar el Numero de Islas");
			System.out.println("4) Mostrar el Tablero");
			System.out.println("5) Limpiar las piezas del Tablero");
			System.out.println("0) Cierro codigo");
			System.out.println();
			
			opcion = sc.nextInt();
			
			switch (opcion) {
			case 1:
				System.out.println("Cuantos peones quieres colocar?: ");
				int peones = sc.nextInt();
				
				while (peones > 0) {
					
					int hey = rdom.nextInt(8);
					int buenas = rdom.nextInt(8);
					
					if (mat[hey][buenas] == 1)  {
						
					} else {
						mat[hey][buenas] = 1;
						peones--;
					}
					
					
				}
				
				System.out.println("COMPLETADO!");
				System.out.println();
				break;
			case 2:
				System.out.println("Los siguientes peones se encuentran en el tablero: ");
				System.out.println();
				for (int r = 0; r < mat.length; r++) {
					for (int c = 0; c < mat.length; c++) {
						if (mat[r][c] == 1) {
							System.out.println(r + " " + c);
						}
					}
				}
				
				System.out.println();
				System.out.println("COMPLETADO!");
				System.out.println();
				break;
			case 3:
				System.out.print("El numero de islas en el tablero es: ");
				
				boolean find1;
				int islas = 0;
				int cont = 0;
				
				for (int c = 0; c < mat.length; c++) {
					
					find1 = false;
					
					for (int r = 0; r < mat.length; r++) {
						
						if (mat[r][c] == 1) {
							find1 = true;
						}										
					}					
										
					if (find1) {
						cont++;
					}
					
					if (!find1 && cont != 0) {
						islas++;
						cont = 0;
					}
					
					if (find1 && c == 7) {
						islas++;
					}
					
					
				}
				System.out.println(islas);
				System.out.println("COMPLETADO!");
				System.out.println();
				break;
			case 4:
				System.out.println("Este es el tablero actual: ");
				for (int r = 0; r < mat.length; r++) {
					for (int c = 0; c < mat.length; c++) {
						System.out.print(mat[r][c] + " ");
					}
					System.out.println();
				}
				System.out.println();
				
				System.out.println("COMPLETADO!");
				System.out.println();
				break;
	
			case 5:
				System.out.println("El tablero esta siendo reiniciado ");
				for (int r = 0; r < mat.length; r++) {
					for (int c = 0; c < mat.length; c++) {
						mat[r][c] = 0;
					} 
				}
				System.out.println("COMPLETADO!");
				System.out.println();
				break;
			}
		
		} while (opcion != 0);
		
		System.out.println("------");
		System.out.println("El programa se ha cerrado exitosamente");
		System.out.println("------");
		
	}

}
