package CodigosGregorio;

import java.util.Scanner;

public class Memory {

	static Scanner sc;

	public static void main(String[] args) {
		
		sc = new Scanner(System.in);
				
		int opcion = -1;
		String[][] tablero = new String[4][4];
		
		do {
			
			menu();
			opcion = sc.nextInt();
			
			switch(opcion) {
			case 1:
				tablero = Iniciar();
				break;			
			case 2:
				Imprimir(tablero);
				break;
			case 3:
				tablero = PonerPiezas(tablero);
				break;
			case 0:
				System.out.println("Hasta la Proxima");
				break;
			default:
				System.out.println("Manito tas equivocao de numero");
			}
						
			
		}while(opcion != 0);
		
	}

	public static void menu() {
		
		System.out.println("---------");
		System.out.println("1. - Inicializar Tablero");
		System.out.println("2. - Mostrar Tablero");
		System.out.println("3. - Poner Piezas Ordenadas");
		System.out.println("---------");
		
	}
	
	public static String[][] Iniciar() {
		
		String[][] tablero = new String[4][4];
		for (int r = 0; r < tablero.length; r++) {
			for (int c = 0; c < tablero.length; c++) {
				tablero[r][c] = "? ";
			}
		}
		
		return tablero;
		
	}
	
	public static void Imprimir(String[][] tablero) {
		
		for (int r = 0; r < tablero.length; r++) {
			for (int c = 0; c < tablero.length; c++) {
				System.out.print(tablero[r][c]);
			}
			System.out.println();
		}
		
	}
	
	public static String[][] PonerPiezas(String[][] tablero) {
		
		int cont = 0;
		for (int r = 0; r < tablero.length; r++) {
			for (int c = 0; c < tablero.length; c++) {
				if (cont < 2) {
					tablero[r][c] = "A ";
					cont++;
				} else if (cont < 4){
					tablero[r][c] = "B ";
					cont++;
				} else if (cont < 6) {
					tablero[r][c] = "C ";
					cont++;
				} else if (cont < 8) {
					tablero[r][c] = "D ";
					cont++;
				} else if (cont < 10) {
					tablero[r][c] = "E ";
					cont++;
				} else if (cont < 12) {
					tablero[r][c] = "F ";
					cont++;
				} else if (cont < 14) {
					tablero[r][c] = "G ";
					cont++;
				}  else if (cont < 16) {
					tablero[r][c] = "H ";
					cont++;
				}
			}
		}
		cont = 0;
		
		return tablero;
	}
}
