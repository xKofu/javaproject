package CodigosGregorio;

import java.util.Scanner;

public class AjedrezRaro {

	public static void main(String[] args) {
				
		Scanner sc = new Scanner(System.in);
		
		int dimension = sc.nextInt();
		String[][] hey = new String[dimension][dimension];
		int cont = 0;
		System.out.println();System.out.println();
		
		for (int r = 0; r < dimension; r++) {
			for (int c = 0; c < dimension; c++) {
				if (cont%2 == 0) {
					hey[r][c]="[+]";
					cont++;
				}
				else {
					hey[r][c]="(-)";
					cont++;
				}
			}
			if (dimension%2 == 0) {
				cont++;	
			}
			
		}
		
		for (int r = 0; r < dimension; r++) {
			for (int c = 0; c < dimension; c++) {
				System.out.print(hey[r][c]);
			}
			System.out.println();
		}
	}
}
