package CodigosGregorio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class Loteria {

	public static void main(String[] args) {
		
		Scanner xd = new Scanner(System.in);
		Random r = new Random();
		
		int opcion = 0;
		int contador = 0;
		
		ArrayList<Integer> combinacion = new ArrayList<>();
		ArrayList<Integer> sorteo = new ArrayList<>();
		ArrayList<Integer> aciertos = new ArrayList<>();
		
		System.out.println("Bienvenido al Sorteo de la Loteria!");
		
		do {
				
			System.out.println("Escoge una de las siguientes opciones:");
			System.out.println("1) Introducir combinación");
			System.out.println("2) Hacer el sorteo");
			System.out.println("3) Comparar la combinación y el sorteo");
			System.out.println("4) Ver Combinación");
			System.out.println("5) Ver Sorteo");
			System.out.println("6) Borrar todo");
			System.out.println("0) Salir");
			
			opcion = xd.nextInt();
			
			switch (opcion) {
			case 1:
				System.out.println("---- Introduce 6 numeros diferentes: ----");
				while (combinacion.size() < 6 ) {
					int c = xd.nextInt();
					if (combinacion.contains(c) || c > 49 || c < 1) {
						System.out.println("Has dado un numero no valido, prueba otra vez");
					} else {
						combinacion.add(c);
					}
					
				}
				System.out.println("---- Muchas Gracias ----");
				break;
			case 2:
				System.out.println("---- Se esta llevando a cabo el sorteo ----");
				
				while (sorteo.size() < 6 ) {
					int s = r.nextInt(48)+1;
					if (sorteo.contains(s)) {
						
					} else {
						sorteo.add(s);
					}
				}
				System.out.println("---- Muchas Gracias ----");
				break;
			case 3:
				System.out.println("---- Se va a realizar el sorteo ----");
				for (int i = 0; i < 6; i++) {
					if (sorteo.contains(combinacion.get(i))) {
						contador++;
						aciertos.add(combinacion.get(i));
					}
				}
				if (contador > 0) {
					System.out.println("---- Felicidades, has acertado " + contador + " numeros!");
					System.out.println("---- Estos han sido tus aciertos: " + aciertos);
					System.out.println("---- Muchas Gracias ----");
					aciertos.clear();
				} else {
					System.out.println("---- No has acertado ninguno... ----");
					System.out.println("---- Muchas Gracias ----");
				}
				break;
			case 4:
				System.out.println("---- Esta es tu combinación ----");
				Collections.sort(combinacion);
				System.out.println(combinacion);
				System.out.println("---- Muchas Gracias ----");
				break;
			case 5:
				System.out.println("---- Esta es el sorteo ----");
				Collections.sort(sorteo);
				System.out.println(sorteo);
				System.out.println("---- Muchas Gracias ----");
				break;
			case 6:
				System.out.println("---- Se ha borrado la combinación ----");
				combinacion.clear();
				System.out.println("---- Se ha borrado el sorteo ----");
				sorteo.clear();
				System.out.println("---- Muchas Gracias ----");
			}
			
				
		} while (opcion != 0);
		
		
	}

}
