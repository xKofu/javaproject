package Tonterias;

import java.util.Scanner;

public class CalcPruebas {

	static Scanner xd;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		xd = new Scanner(System.in);
		
		
		//leeremos dos numeros y haremos con ellos operaciones logicas
		//menu
		
		int opcion = -1;
		int num1 = 0;
		int num2 = 0;
			
		do {
			menu();
			opcion = xd.nextInt();
			switch (opcion) {
				case 1:
					System.out.println("Entras 1 \n");
					
					num1 = obtenerNumero();
	
					System.out.println("Sales 1");
					break;
				case 2:
					System.out.println("Entras 2");
					
					num2 = obtenerNumero();
					
					System.out.println("Sales 2");
					break;
				case 3:
					System.out.println("Entras 3");
					
					num1 = obtenerNumero();
					num2 = obtenerNumero();
					
					
					System.out.println("Sales 3");
					break;
				case 4: // miro los numeros
					System.out.println("Entras 4");
					
					vernumeros(num1, num2);
					
					//System.out.println("Numero 1: " + num1 + " Numero 2: " + num2);
					
					
					System.out.println("Sales 4");
					break;
				case 5:
					System.out.println("Entras 5");
					
					System.out.println("el mas grande es " + masGrande(num1, num2));
					
					
					System.out.println("Sales 5");
					break;
				case 6:
					System.out.println("Entras 6");
					
					System.out.println("el promedio es " + promedio(num1, num2));
					
					
					System.out.println("Sales 6");
					break;
				case 0:
					System.out.println("Hasta la proximaaa!!");
					break;
						
			}
			
		}while (opcion != 0);
	}

	private static void menu() {
		System.out.println("Programa para ver funciones");
		System.out.println("***************************\n\n");
		System.out.println("1.- Obtener primer numero");
		System.out.println("2.- Obtener Segundo numero");
		System.out.println("3.- Obtener dos numeros");
		System.out.println("4.- ver los numeros");
		System.out.println("5.- Saber cual es mas grande");
		System.out.println("6.- Promedio");
		System.out.println("0.- Salir");
		
		
	}
	
	private static int obtenerNumero() {
		
		System.out.print("Dime un numero: ");
		return(xd.nextInt());
	}
	
	private static void vernumeros(int num1, int num2) {
		System.out.println("Numero 1: " + num1 + " Numero 2: " + num2);
	}
	
	private static int masGrande(int num1, int num2) {
		if (num1 > num2) 
			return num1;
		else
			return num2;
	}
	
	private static double promedio(int num1, int num2) {
		return((double)(num1 + num2)) / 2.0;
	}
}
