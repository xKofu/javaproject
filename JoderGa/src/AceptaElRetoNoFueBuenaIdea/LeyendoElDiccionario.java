package AceptaElRetoNoFueBuenaIdea;

import java.util.Scanner;

public class LeyendoElDiccionario {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int caso;

		do {

			int s = 0;
			int def;
			caso = sc.nextInt();

			if (caso == 0) {
				break;
			}

			do { 
				def = sc.nextInt();
				s += (def * caso);
			} while (def != 0);

			int h = 0;
			int m = 0;

			while (s > 60) {
				s -= 60;
				m++;
			}

			while (m > 60) {
				m -= 60;
				h++;
			}

			System.out.printf("%0d:%0d:%0d", h, m, s);
			
			/*if (h < 10) {
				System.out.print("0"+h+":");
			} else {
				System.out.print(h+":");
			}
			if (m < 10) {
				System.out.print("0"+m+":");
			} else {
				System.out.print(m+":");
			}
			if (s < 10) {
				System.out.print("0"+s);
			} else {
				System.out.print(s);
			}
			System.out.println();*/

		} while (caso != 0);
	}
}
