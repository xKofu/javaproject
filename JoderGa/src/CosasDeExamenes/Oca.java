package CosasDeExamenes;

import java.util.Random;
import java.util.Scanner;

public class Oca {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Inserta una Opcion: ");

		
		int intro = sc.nextInt();
		int[] array = new int[32];
		int posicionuser = 0;
		int cas1 = 0;
		int cas2 = 0;
		Random r = new Random();

		while (intro != 0) {
			
			System.out.println("1 - Dejar a 0 ");
			System.out.println("2 - Emparejar casillas ");
			System.out.println("3 - Ver tablero");
			System.out.println("4 - Tirar dado");
			System.out.println("0 - Salir");
			
			switch (intro) {
			case 1:
				System.out.println("--- Opcion 1 Completada ---");
				array = new int[32];
				posicionuser = 0;
				System.out.println("Inserta una Opcion: ");
				intro = sc.nextInt();
				break;
			case 2:
				System.out.println("Inserta la primera casilla: ");
				cas1 = sc.nextInt()-1;
				System.out.println("Inserta la segunda casilla ");
				cas2 = sc.nextInt()-1;
				if (array[cas1] == 0 && array[cas2] == 0) {
					array[cas1] = cas2;
					array[cas2] = cas1;
				} else {
					System.err.println("Fatal Error");
				}
				System.out.println("Inserta una Opcion: ");
				intro = sc.nextInt();
				break;
			case 3:
				for (int i = 0; i < array.length; i++) {
					if (i == posicionuser) {
						System.out.print("[" + array[i] + " X ] ");
					}else if (i > 0 && i == cas1){
						System.out.print("[" + array[i] + "(" + array[cas2] + ")] ");
					}else if (i > 0 && i == cas2){
						System.out.print("[" + array[i] + "(" + array[cas1] + ")] ");
					} else {
						System.out.print("[" + array[i] + "] ");
					}
				}
				System.out.println();
				System.out.println("Inserta una Opcion: ");
				intro = sc.nextInt();
				break;
			case 4:
				System.out.println("Tu posicion actual es: " + posicionuser);
				int dado = r.nextInt(6)+1;
				System.out.println("Has tirado un dado, te ha salido: " + dado);
				posicionuser += dado;
				if (posicionuser > 32) {
					posicionuser = 32;
				} 
				if (posicionuser == cas1) {
					posicionuser = cas2;
				} else if (posicionuser == cas2) {
					posicionuser = cas1;
				}
				if (posicionuser == 32) {
					System.out.println("Tu nueva posicion es: " + posicionuser);
					System.out.println("Enhorabuena, has ganado!");
					intro = 0;
				} else {
					System.out.println("Tu nueva posicion es: " + posicionuser);
					System.out.println("Inserta una Opcion: ");
					intro = sc.nextInt();
				}
				break;
			}
			
		}
		System.out.println("--El juego ha finalizado--");
		
	}

}
