package CalculadoraJUnit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculadoraTest {

	@Test
	void testSuma() {
		assertEquals(30, Calculadora.suma(10, 20));
		assertEquals(45, Calculadora.suma(15, 30));
		assertEquals(120, Calculadora.suma(25, 95));
		assertEquals(43, Calculadora.suma(12, 31));
		assertEquals(-20, Calculadora.suma(10, -30));
	}
	
	@Test
	void testResta() {
		assertEquals(30, Calculadora.resta(40, 10));
		assertEquals(45, Calculadora.resta(40, -5));
		assertEquals(120, Calculadora.resta(300, 180));
		assertEquals(43, Calculadora.resta(-43, -86));
		assertEquals(-20, Calculadora.resta(-10, 10));
	}
	
	@Test
	void testMultiplicacion() {
		assertEquals(120, Calculadora.multiplicacio(40, 3));
		assertEquals(36, Calculadora.multiplicacio(3, 12));
		assertEquals(828, Calculadora.multiplicacio(46, 18));
		assertEquals(8, Calculadora.multiplicacio(2, 4));
		assertEquals(-80, Calculadora.multiplicacio(2, -40));
	}
	
	@Test
	void testDivision() {
		assertEquals(5, Calculadora.divisio(10, 2,'D'));
		assertEquals(2, Calculadora.divisio(30, 15,'D'));
		assertEquals(2, Calculadora.divisio(300, 135,'D'));
		assertEquals(4, Calculadora.divisio(234, 5,'M'));
		assertEquals(0, Calculadora.divisio(12, 3,'M'));
		assertEquals(2, Calculadora.divisio(20, 6,'M'));
	} 
}
