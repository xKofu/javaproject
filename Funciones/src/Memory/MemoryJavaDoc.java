package Memory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class MemoryJavaDoc {

	static Scanner sc;
	static Random rdom;
	static int puntos1 = 0;
	static int puntos2 = 0;
	
	/**
	 * MEMORY
	 * @param args ??
	 */
	
	public static void main(String[] args) {
		
		sc = new Scanner(System.in);
		rdom = new Random();
		
		boolean superflag = true;
		boolean acabar = false;
		int loquetecuento1 = 0;
		int loquetecuento2 = 0;
		boolean jugada = rdom.nextBoolean();
		int contint = 0;
		
		System.out.println("---BIENVENIDO AL JUEGO ESTE DE MIERDA---");
		System.out.println("Dime el nombre del jugador 1: ");
		String Jugador1 = sc.nextLine();
		System.out.println("Dime el nombre del jugador 2: ");
		String Jugador2 = sc.nextLine();
		System.out.println("---------");
		System.out.println("El primer turno se decidira aleatoriamente!");
		System.out.println("---------");
		
		
		
		int opcion = -1;
		String[][] tablero = new String[4][4];
		String[][] tableroS = new String[4][4];
		tablero = Iniciar();
		
		do {
			if (jugada) {
				System.out.println("Turno de " + Jugador1);
				System.out.println("Buena suerte");
			} else {
				System.out.println("Turno de " + Jugador2);
				System.out.println("Buena suerte");
			}
			menu();
			opcion = sc.nextInt();
			acabar = false;
			contint = 0;
			
			switch(opcion) {
			case 1:
				tableroS = Iniciar(); 
				break;			
			case 2:
				Imprimir(tablero);
				break;
			case 3:
				tableroS = PonerPiezas(tableroS);
				break;
			case 4:
				tableroS = Desordenar(tableroS);
				break;
			case 5:
				if (superflag) {
					System.out.println();
					System.out.println("->A que carta le quieres dar la vuelta?: ");
					int posr = sc.nextInt();
					int posc = sc.nextInt();
					superflag = Pedir1(tablero, tableroS, superflag, posr, posc);
					loquetecuento1 = posr;
					loquetecuento2 = posc;
				} else {
					System.out.println();
					System.out.println("->Ya hay una carta volteada, que otra carta quieres girar :)?: ");
					int posr = sc.nextInt();
					int posc = sc.nextInt();
					superflag = Pedir2(tablero, tableroS, superflag, posr, posc, loquetecuento1, loquetecuento2, jugada);
					jugada = !jugada;
				}
				break;
			case 8:
				Imprimir(tableroS);
				break;
			case 0:
				System.out.println("Estas a punto de Salir del Juego, estas seguro? ");
				System.out.println("Pulsa 0 para Si / Pulsa 1 Para No");
				opcion = sc.nextInt();
				if (opcion == 0) {
					System.out.println("--> Hasta la Proxima <--");
				} else {
					System.out.println("--> El Juego Continua <--");
				}
			case 9:
				System.out.println("Acabado Forzado");
				tablero = tableroS;
			default:
				System.out.println("HMMM Manito tas equivocao de numero, que no vuelva a pasar");
			}
						
			contint = Comprovar(tablero, contint);
			
		}while(contint != 0);
		
		System.out.println("Puntuación Final: ");
		System.out.println(Jugador1+": "+puntos1);
		System.out.println(Jugador2+": "+puntos2);
		
	}

	/**
	 * Menu del Juego
	 */
	
	public static void menu() {
		
		System.out.println("---------");
		System.out.println("1. - Inicializar Tablero");
		System.out.println("2. - Mostrar Tablero");
		System.out.println("3. - Poner Piezas Ordenadas");
		System.out.println("4. - Desordenar Piezas");
		System.out.println("5. - Darle la vuelta a una carta");
		System.out.println("0. - Cerrar Programa");
		System.out.println("8. - Test");
		System.out.println("---------");
		
	}
	
	/**
	 * Comprueva que el tablero este vacio.
	 * @param tab Matriz con el tablero
	 * @param fin contador
	 * @return numero de casillas sin jugar
	 */
	
	public static int Comprovar(String[][] tab, int fin) {
		
		for (int r = 0; r < tab.length; r++) {
			for (int c = 0; c < tab.length; c++) {
				if (tab[r][c].equals("?")) {
					fin++;
				}
			}
		}
		
		return fin;
		
	}
	
	/**
	 * Creamos el tablero de Juego
	 * @return Una matriz con el tablero de juego
	 */
	
	public static String[][] Iniciar() {
		
		String[][] tablero = new String[4][4];
		for (int r = 0; r < tablero.length; r++) {
			for (int c = 0; c < tablero.length; c++) {
				tablero[r][c] = "?";
			}
		}
		
		return tablero;
		
	}
	
	/**
	 * La usamos para visualizar el tablero por pantalla
	 * @param paco Matriz a visualizar
	 */
	
	public static void Imprimir(String[][] paco) {
		System.out.println("<-------->");
		for (int r = 0; r < paco.length; r++) {
			System.out.print(r + " ");
			for (int c = 0; c < paco.length; c++) {
				System.out.print(paco[r][c] + " ");
			}
			System.out.println();
		}
		System.out.println("  0 1 2 3");
		System.out.println("<-------->");
		System.out.println();
	}
	
	/**
	 * Pone las piezas ordenadas en el tablero
	 * @param tablero tablero en el que queremos poner las piezas
	 * @return tablero con las piezas puestas
	 */
	
	public static String[][] PonerPiezas(String[][] tablero) {
		
		ArrayList<String> llista = new ArrayList(Arrays.asList("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","X","Y","Z"));
		boolean flag = false;
		int i = 0;
		for (int r = 0; r < tablero.length; r++) {
			for (int c = 0; c < tablero.length; c++) {
			
				tablero[r][c] = llista.get(i);
				if(flag) {
					i++;	
				}
				flag = !flag;
				if (i == llista.size()) {
					i = 0;
				}
			}
		}
		
		return tablero;
	}
	
	/**
	 * Desordena las piezas del tablero
	 * @param tablero Matriz a desordenar
	 * @return Matriz desordenada
	 */
	
	public static String[][] Desordenar(String[][] tablero) {
		
		rdom = new Random();
		
		int r1;
		int r2;
		int aux1 = 0;
		int aux2 = 0;
		String auxf;
		
		for (int i = 0; i < 50; i++) {
			r1 = rdom.nextInt(4);
			r2 = rdom.nextInt(4);
			aux1 = r1;
			aux2 = r2;
			auxf = tablero[aux1][aux2];
			r1 = rdom.nextInt(4);
			r2 = rdom.nextInt(4);
			tablero[aux1][aux2] = tablero[r1][r2];
			tablero[r1][r2] = auxf;
		}
		
		return tablero;
	}
	
	/**
	 * Pedimos la primera posicion a revisar
	 * @param tablero Tablero de juego
	 * @param Secreto Tablero con los resultados
	 * @param superflag Un flag para saber si ya se ha revisado la primera pieza
	 * @param posr posicion de la fila a revisar
	 * @param posc posicion de la columna a revisar
	 * @return Un flag para saber que funcion toca hacer en cada momento
	 */
	
	public static boolean Pedir1(String[][] tablero, String [][] Secreto, boolean superflag, int posr, int posc) {
		
			if (!tablero[posr][posc].equals("?")) {
				System.out.println();
				System.out.println("---MAL - Esa pieza ya esta pillada---");
				System.out.println();
			} else {
				tablero[posr][posc] = Secreto[posr][posc];
				System.out.println();
				Imprimir(tablero);
				superflag = false;	
			}
			
			return superflag;
		} 
	
	/**
	 * Pedimos la segunda pieza y la comparamos con la primera
	 * @param tablero Tablero de juego
	 * @param Secreto Tablero con los resultados
	 * @param superflag Un flag para saber si ya se ha revisado la primera pieza
	 * @param posr posicion de la fila a revisar
	 * @param posc posicion de la columna a revisar
	 * @param loquetecuento1 posicion de la fila de la primera pieza
	 * @param loquetecuento2 posicion de la columna de la primera pieza
	 * @param jugada Flag para saber que jugador esta ganando un punto
	 * @return Un flag para saber que funcion toca hacer en cada momento
	 */
	
	public static boolean Pedir2(String[][] tablero, String [][] Secreto, boolean superflag, int posr, int posc, int loquetecuento1, int loquetecuento2, boolean jugada) {
		
			if (!tablero[posr][posc].equals("?")) {
				System.out.println();
				System.out.println("---MAL - Esa pieza ya esta pillada---");
				System.out.println();
			} else {
				tablero[posr][posc] = Secreto[posr][posc];
				System.out.println();
				Imprimir(tablero);
				if (tablero[posr][posc].equals(tablero[loquetecuento1][loquetecuento2])) {
					System.out.println();
					System.out.println("--->Enhorabuena! Has acertado!");
					if (jugada) {
						puntos1++;
					} else {
						puntos2++;
					}
					System.out.println();
					superflag = true;
				} else {
					System.out.println();
					System.out.println("--->Una pena...");
					System.out.println();
					tablero[posr][posc] = "?";
					tablero[loquetecuento1][loquetecuento2] = "?";
					superflag = true;
				}
			}
			
			return superflag;
		}
		
	}
