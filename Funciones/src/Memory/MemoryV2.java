package Memory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class MemoryV2 {

	static Scanner sc;
	static Random rdom;
	
	public static void main(String[] args) {
		
		sc = new Scanner(System.in);
		rdom = new Random();
		
		boolean superflag = true;
		boolean iniciar = false;
		boolean jugada = rdom.nextBoolean();
		
		int opcion = -1;
		String[][] tablero = new String[4][4];
		String[][] tableroS = new String[4][4];
		int puntos1 = 0;
		int puntos2 = 0;
		
		do {
			
			if (iniciar) {
				if (jugada) {
					System.out.println("Turno de Jugador 1");
					System.out.println("Buena suerte");
					jugada = false;
				} else {
					System.out.println("Turno de Jugador 2");
					System.out.println("Buena suerte");
					jugada = true;
				}
			}
						
			menu();
			opcion = sc.nextInt();
			
			switch(opcion) {
			case 1:
				tablero = Iniciar();
				tableroS = Desordenar(PonerPiezas(tableroS));
				iniciar = true;
				System.out.println();				
				break;
			case 2:
				if (iniciar) {
					Imprimir(tablero);
					System.out.println();	
				} else {
					System.out.println("Primero Tienes que Iniciar el Juego!");
				}
				jugada = !jugada;
				break;
			case 3:
				if (iniciar) {
					
				} else {
					System.out.println("Primero Tienes que Iniciar el Juego!");
				}
				jugada = !jugada;
				break;
			case 4:
				if (iniciar) {
					System.out.println("--------");
					System.out.println("Esta es la Puntuacion Actual");
					System.out.println("Jugador 1 -> " + puntos1);
					System.out.println("jugador 2 -> " + puntos2);
					System.out.println("--------");
					System.out.println();
				} else {
					System.out.println("Primero Tienes que Iniciar el Juego!");
				}
				jugada = !jugada;
				break;
			case 5:
				if (iniciar) {
					
				} else {
					System.out.println("Primero Tienes que Iniciar el Juego!");
				}
				break;
			case 8:
				if (iniciar) {
				Imprimir(tableroS);
				} else {
					System.out.println("Primero Tienes que Iniciar el Juego!");
				}
				jugada = !jugada;
				break;
			case 0:
				System.out.println("Estas a punto de Salir del Juego, estas seguro? ");
				System.out.println("Pulsa 0 para Si / Pulsa 1 Para No");
				opcion = sc.nextInt();
				if (opcion == 0) {
					System.out.println("--> Hasta la Proxima <--");
				} else {
					System.out.println("--> El Juego Continua <--");
				}
				break;
			default:
				System.out.println("HMMM Manito tas equivocao de numero, que no vuelva a pasar");
			}
						
			
		}while(opcion != 0);
		
	}

	public static void menu() {
		
		System.out.println("---------");
		System.out.println("1. - Inicializar Juego");
		System.out.println("2. - Mostrar Tablero");
		System.out.println("3. - Pasar Turno");
		System.out.println("4. - Mostrar Puntuacion");
		System.out.println("5. - Jugar Turno");
		System.out.println("0. - Cerrar Programa");
		System.out.println("8. - Trampas");
		System.out.println("---------");
		
	}
	
	public static String[][] Iniciar() {
		
		String[][] tablero = new String[4][4];
		for (int r = 0; r < tablero.length; r++) {
			for (int c = 0; c < tablero.length; c++) {
				tablero[r][c] = "?";
			}
		}
		
		return tablero;
		
	}
	
	public static void Imprimir(String[][] paco) {
		System.out.println("<-------->");
		for (int r = 0; r < paco.length; r++) {
			System.out.print(r + " ");
			for (int c = 0; c < paco.length; c++) {
				System.out.print(paco[r][c] + " ");
			}
			System.out.println();
		}
		System.out.println("  0 1 2 3");
		System.out.println("<-------->");
		System.out.println();
	}
	
	public static String[][] PonerPiezas(String[][] tablero) {
		
		ArrayList<String> llista = new ArrayList(Arrays.asList("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","X","Y","Z"));
		boolean flag = false;
		int i = 0;
		for (int r = 0; r < tablero.length; r++) {
			for (int c = 0; c < tablero.length; c++) {
			
				tablero[r][c] = llista.get(i);
				if(flag) {
					i++;	
				}
				flag = !flag;
				if (i == llista.size()) {
					i = 0;
				}
			}
		}
		
		return tablero;
	}
	
	public static String[][] Desordenar(String[][] tablero) {
		
		rdom = new Random();
		
		int r1;
		int r2;
		int aux1 = 0;
		int aux2 = 0;
		String auxf;
		
		for (int i = 0; i < 50; i++) {
			r1 = rdom.nextInt(4);
			r2 = rdom.nextInt(4);
			aux1 = r1;
			aux2 = r2;
			auxf = tablero[aux1][aux2];
			r1 = rdom.nextInt(4);
			r2 = rdom.nextInt(4);
			tablero[aux1][aux2] = tablero[r1][r2];
			tablero[r1][r2] = auxf;
		}
		
		return tablero;
	}
	
}
