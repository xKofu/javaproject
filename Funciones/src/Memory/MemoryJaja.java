package Memory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class MemoryJaja {

	static Scanner sc;
	static Random rdom;
	
	public static void main(String[] args) {
	
		sc = new Scanner(System.in);
		
		int opcion = 0;
				
		do {
			
			menu();
			opcion = sc.nextInt();
			switch (opcion) {
			case 1:
				PartidaInd();
				break;
			case 2:
				Partida2player();
				break;
			case 0:
				opcion = Cuidao(opcion);
				break;
			}
			
		} while (opcion != 0);
		
	}

	private static void PartidaInd() {
		
		String[][] Tablero = new String[4][4];
		String[][] TableroS = new String[4][4];
		
		Tablero = IniciarTablero();
		TableroS = Desordenar(PonerPiezas(TableroS));
		
		System.out.println("----------------------");
		System.out.println("| Partida Indivudual |");
		System.out.println("----------------------");
		
		int opcion = 0;
		
		do {
			
			menuInd();
			opcion = sc.nextInt();
			
			switch (opcion) {
			case 1:
				DestaparCartas(Tablero, TableroS);
				ComprovarCartas(Tablero);
				
				break;
			case 2:
				Imprimir(Tablero);
				break;
			case 0:
				opcion = Cuidao(opcion);
				break;
			}
			
		}while(opcion != 0);
		
	}
	

	private static void ComprovarCartas(String[][] tablero) {
		// TODO Auto-generated method stub
		
	}

	private static void DestaparCartas(String[][] tablero, String[][] tableroS) {
		// TODO Auto-generated method stub
		
	}

	private static void Partida2player() {
		
		
		
	}

	private static int Cuidao(int opcion) {
		
		System.out.println("Estas a punto de Salir del Juego, estas seguro? ");
		System.out.println("Pulsa 0 para Si / Pulsa 1 Para No");
		opcion = sc.nextInt();
		if (opcion == 0) {
			System.out.println("--> Hasta la Proxima <--");
		} else {
			System.out.println("--> El Juego Continua <--");
		}
		return opcion;
	}

	private static void menu2player() {

		System.out.println("---------");
		System.out.println("1. - Nombrar Jugadores");
		System.out.println("2. - Pasar turno");
		System.out.println("3. - Jugar turno");
		System.out.println("4. - Mostrar tablero");
		System.out.println("5. - Mostrar puntuaciones");
		System.out.println("0. - Acabar partida");
		System.out.println("---------");
		
	}

	private static void menuInd() {
		
		System.out.println("---------");
		System.out.println("1. - Jugar turno");
		System.out.println("2. - Mostrar tablero");
		System.out.println("0. - Acabar partida");
		System.out.println("---------");
		
		
	}

	private static void menu() {
		
		System.out.println("---------");
		System.out.println("Numero de jugadores?: ");
		System.out.println("1 Jugador");
		System.out.println("2 Jugadores");
		System.out.println("---------");
		
	}

	public static String[][] PonerPiezas(String[][] tablero) {
		
		ArrayList<String> llista = new ArrayList(Arrays.asList("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","X","Y","Z"));
		boolean flag = false;
		int i = 0;
		for (int r = 0; r < tablero.length; r++) {
			for (int c = 0; c < tablero.length; c++) {
			
				tablero[r][c] = llista.get(i);
				if(flag) {
					i++;	
				}
				flag = !flag;
				if (i == llista.size()) {
					i = 0;
				}
			}
		}
		
		return tablero;
	}

	public static String[][] IniciarTablero() {
		
		String[][] tablero = new String[4][4];
		for (int r = 0; r < tablero.length; r++) {
			for (int c = 0; c < tablero.length; c++) {
				tablero[r][c] = "?";
			}
		}
		
		return tablero;
		
	}
	
	public static String[][] Desordenar(String[][] tablero) {
		
		rdom = new Random();
		
		int r1;
		int r2;
		int aux1 = 0;
		int aux2 = 0;
		String auxf;
		
		for (int i = 0; i < 50; i++) {
			r1 = rdom.nextInt(4);
			r2 = rdom.nextInt(4);
			aux1 = r1;
			aux2 = r2;
			auxf = tablero[aux1][aux2];
			r1 = rdom.nextInt(4);
			r2 = rdom.nextInt(4);
			tablero[aux1][aux2] = tablero[r1][r2];
			tablero[r1][r2] = auxf;
		}
		
		return tablero;
	}
	
	public static void Imprimir(String[][] paco) {
		System.out.println("<-------->");
		for (int r = 0; r < paco.length; r++) {
			System.out.print(r + " ");
			for (int c = 0; c < paco.length; c++) {
				System.out.print(paco[r][c] + " ");
			}
			System.out.println();
		}
		System.out.println("  0 1 2 3");
		System.out.println("<-------->");
		System.out.println();
	}
	
}
