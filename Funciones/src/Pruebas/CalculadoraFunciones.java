package Pruebas;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class CalculadoraFunciones {

	static Scanner sc;
	
	public static void main(String[] args) throws InterruptedException {
		
		sc = new Scanner(System.in);
		
		int opcion = -1;
		int hey = 0;
		int buenas = 0;
		boolean flag = false;
		boolean res = false;
		
		do {
			
			menu();
			opcion = sc.nextInt();
			
			
			switch (opcion) {
			case 1: 
				System.out.println();
				if (!flag) {
					hey = PedirNumero();
					buenas = PedirNumero();
					flag = true;
				} else {
					buenas = PedirNumero();
				}
				break;
			case 2: 
				System.out.println("---------");
				System.out.println();
				System.out.println("El resultado de la suma es: " + Sumar(hey, buenas));
				hey = Sumar(hey, buenas);
				res = true;
				System.out.println();
				break;
			case 3: 
				System.out.println("---------");
				System.out.println();
				System.out.println("El resultado de la suma es: " + Restar(hey, buenas));
				hey = Restar(hey, buenas);
				res = true;
				System.out.println();
				break;
			case 4: 
				System.out.println("---------");
				System.out.println();
				System.out.println("El resultado de la suma es: " + Mult(hey, buenas));
				hey = Mult(hey, buenas);
				res = true;
				System.out.println();
				break;
			case 5: 
				menu2();
				int opcion2 = sc.nextInt();
				switch(opcion2) {
				case 1:
					System.out.println("---------");
					System.out.println();
					System.out.println("El resultado de la suma es: " + Res(hey, buenas));
					hey = Res(hey, buenas);
					res = true;
					System.out.println();
					break;
				case 2:
					System.out.println("---------");
					System.out.println();
					System.out.println("El resultado de la suma es: " + Div(hey, buenas));
					hey = Div(hey, buenas);
					res = true;
					System.out.println();
					break;
				default:
					System.out.println("El numero proporcionado no es una de las opciones...");
				}
				break;
			case 6: 
				if (res) {
					System.out.println("El resultado actual es: "+hey);
				} else {
					System.out.println("Aun no se ha realizado ninguna operacion!");
				}
				break;
			case 0: 
					System.out.println("Gracias por Utilizar la Calculadora!");
				break;
			default:
				System.out.println("Maquina creo que no es tan dificil ver que el numero que me has dado no es una de las opciones que salen en el menu");
			}
			
			
		}while (opcion != 0);
		
	}
	
	public static void menu() {
		
		System.out.println("---------");
		System.out.println("1. - Obtener numeros a Operar");
		System.out.println("2. - Sumar");
		System.out.println("3. - Restar");
		System.out.println("4. - Multiplicar");
		System.out.println("5. - Division");
		System.out.println("6. - Visualizar el resultado");
		System.out.println("0. - Salir del Programa");
		System.out.println("---------");
		
	}
	
	public static void menu2() {
		
		System.out.println("---------");
		System.out.println("1. - Obtener el Resto");
		System.out.println("2. - Obtener el Residuo");
		System.out.println("---------");
		
	}
	
	private static int PedirNumero() {
	
		System.out.println("Dime un numero: ");
		return(sc.nextInt());
	
	}
	
	private static int Sumar(int num1, int num2) {
		
		num1 += num2;
		return num1;
		
	}
	
	private static int Restar(int num1, int num2) {
		
		num1 -= num2;
		return num1;
		
	}
	
	private static int Mult(int num1, int num2) {
		
		num1 *= num2;
		return num1;
		
	}

	private static int Div(int num1, int num2) {
	
		num1 = num1 / num2;
		return num1;
	
	}

	private static int Res(int num1, int num2) {
		
		num1 = num1 % num2;
		return num1;
		
	}
	
	
}
