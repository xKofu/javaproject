package Pruebas;

import java.util.Random;
import java.util.Scanner;

public class PruebaImprimir {

	static Scanner sc;
	static Random rdom;
	
	public static void main(String[] args) {
		
		sc = new Scanner(System.in);
		rdom = new Random();
		
		// Variables que tienen que estar antes de que empiece un por la cara hmm
		
		// Tablero
		boolean jugar = true; // Boolean
		
		System.out.println("╔══════════╗");
		System.out.println("║ IMPRIMIR ║");
		System.out.println("╚══════════╝");
		System.out.println();
		
		
		menu();
		while (jugar) {
			
		
		int opcion = sc.nextInt();
		switch(opcion) {
		case 1:
			String[][] pr = new String[rdom.nextInt(9)+1][rdom.nextInt(9)+1];
			Prueba(pr);
			Imprimir(pr);
			break;
		case 0:
			jugar = false;
			break;
		}
		}
		
	}

	private static void Prueba(String[][] pr) {
		
		for (int i = 0; i < pr.length; i++) {
			for (int j = 0; j < pr[0].length; j++) {
				pr[i][j] = "X";
			}
		}
		
	}

	private static void menu() {
		
		System.out.println("---------");
		System.out.println("1. - Imprimir");
		System.out.println("---------");
		
	}

	public static void Imprimir(String[][] paco) {

		for (int r = 0; r < paco.length; r++) {
			System.out.print(r + " ║ ");
			for (int c = 0; c < paco[0].length; c++) {
				System.out.print(paco[r][c] + " ");
			}
			System.out.println();
		}
		System.out.print("  ╚═");
		for (int i = 0; i < paco[0].length; i++) {
			System.out.print("══");
		}
		System.out.println();
		System.out.print("    ");
		for (int i = 0; i < paco[0].length; i++) {
			System.out.print(i + " ");
		}
		System.out.println();
	}
}
