package Buscaminas;

import java.util.Random;
import java.util.Scanner;

public class BuscaminasV2 {

	static Scanner sc;
	static Random rdom;

	public static void main(String[] args) {

		sc = new Scanner(System.in);
		rdom = new Random();

		boolean jugar = true;
		int[][] TableroMinas = null;
		int[][] Tablero = null;

		System.out.println("╔════════════╗");
		System.out.println("║ BUSCAMINAS ║");
		System.out.println("╚════════════╝");
		System.out.println();

		menu();

		while (jugar) {

			int opcion = sc.nextInt();
			sc.nextLine();

			switch (opcion) {
			case 1:
				Ayuda();
				break;
			case 2:
				Opciones();
				TableroMinas = new int[Opciones.filas][Opciones.columnas];
				Tablero = new int[Opciones.filas][Opciones.columnas];
				IniciarMinas(TableroMinas);
				IniciarTablero(Tablero);
				break;
			case 3:
				if (Opciones.conf) {
					Imprimir(Tablero);
					jugar = Jugar(Tablero, TableroMinas);
				} else {
					System.out.println("Amigo no has generado tablas");
				}
				break;
			case 4:
				VerRanking();
				break;
			case 5:
				System.out.println("Imprimir");
				Imprimir(Tablero);
				break;
			case 0:
				jugar = Cuidao();
				break;
			}
		}

	}

	private static boolean Jugar(int[][] tablero, int[][] tableroMinas) {

		boolean resultado = true;
		
		// Pedir posicion
		// Mirar si es valida
		// Mirar si es mina
			// Si es Acaba el juego por espabilao
			// Si no llamamos funcion de mirar a las minas de alrededor
		
		System.out.println("Que posicion quieres destapar?: ");
		int FilaD = sc.nextInt();
		int ColD = sc.nextInt();
		
		if (tablero[FilaD][ColD] == Datos.TAPADO) {
			boolean aux = true;
			while (aux) {
				
				System.out.println("Quieres Destapar (0) o Marcar (1)? ");
				int opcion = sc.nextInt();
				switch(opcion) {
				case 0:
					
					if (tableroMinas[FilaD][ColD] == Datos.MINA) {
						resultado = false;
						tablero[FilaD][ColD] = Datos.BOMBAZO;
						System.out.println(" --  HAS ENCONTRADO UNA MINA, YA ME JODERIA SER TU --");
					} else {
						
						Mirar(tablero, tableroMinas, FilaD, ColD);
						
						
					}
					aux = false;
					break;
				case 1:
					
					tablero[FilaD][ColD] = Datos.MARCADO;
					aux = false;
					
					break;
				default:
					System.out.println("No has seleccionado una opcion valida");
					System.out.println();
				}
			}
			
		}else {
			System.out.println("Manito esa posicion no es valida");
		}
		
		return resultado;

	}

	private static void Mirar(int[][] tablero, int[][] tableroMinas, int row, int col) {
		
		if (tablero[row][col] != Datos.TAPADO) {
			return;
		}
		
		int bomb = 0;
		
		if (row > 0 && col > 0) 
			if (tableroMinas[row-1][col-1] == Datos.MINA) bomb++;
		if (row > 0) 
			if (tableroMinas[row-1][col] == Datos.MINA) bomb++;
		if (row  >0 && col < tablero.length - 1) 
			if (tableroMinas[row-1][col+1] == Datos.MINA) bomb++;
		
		if (col > 0)
			if (tableroMinas[row][col-1] == Datos.MINA)	bomb++;
		if (col < tablero[0].length-1)
			if (tablero[row][col+1] == Datos.MINA) bomb++;
		
		if (row < tablero.length-1 && col > 0) 
			if (tableroMinas[row+1][col-1] == Datos.MINA) bomb++;
		if (row < tablero.length-1) 
			if (tableroMinas[row+1][col] == Datos.MINA) bomb++;
		if (row < tablero.length-1 && col < tablero[0].length-1) 
			if (tableroMinas[row+1][col+1] == Datos.MINA) bomb++;
		
		tablero[row][col] = bomb;
		
		if (bomb == 0) {
			if (row > 0 && col > 0) 									Mirar(tablero, tableroMinas, row-1, col-1);
			if (row > 0)												Mirar(tablero, tableroMinas, row-1, col);
			if (row > 0 && col < tablero.length - 1)					Mirar(tablero, tableroMinas, row-1, col+1);
			if (col > 0)												Mirar(tablero, tableroMinas, row, col-1);
			if (col < tablero[0].length-1)								Mirar(tablero, tableroMinas, row, col+1);
			if (row < tablero.length-1 && col > 0)						Mirar(tablero, tableroMinas, row+1, col-1);
			if (row < tablero.length-1) 								Mirar(tablero, tableroMinas, row+1, col);
			if (row < tablero.length-1 && col < tablero[0].length-1) 	Mirar(tablero, tableroMinas, row+1, col+1);
		}
		
	}

	private static void IniciarTablero(int[][] tablero) {

		for (int r = 0; r < tablero.length; r++) {
			for (int c = 0; c < tablero[0].length; c++) {
				tablero[r][c] = Datos.TAPADO;
			}
		}

		System.out.println("Tablero: ");
		Imprimir(tablero);
		System.out.println();
	}

	private static void VerRanking() {

		System.out.println();
		System.out.println("╔═══════════════════════");
		System.out.println("║ -> " + Opciones.nombre + " tiene una puntuacion de: " + Opciones.puntuacion);
		System.out.println("╚═══════════════════════");

	}

	private static void IniciarMinas(int[][] tableroMinas) {

		int cont = Opciones.minas;

		while (cont > 0) {
			int r1 = rdom.nextInt(Opciones.filas);
			int r2 = rdom.nextInt(Opciones.columnas);

			if (tableroMinas[r1][r2] == Datos.MINA) {

			} else {
				tableroMinas[r1][r2] = Datos.MINA;
				cont--;
			}
		}
		System.out.println("Minas: ");
		Imprimir(tableroMinas);
		System.out.println();

	}

	private static void Opciones() {

		// La posicion 0 seran las filas del tablero
		// la posicion 1 seran las columnas del tablero
		// la posicion 2 seran las minas a colocar

		System.out.println();
		System.out.println("Nombre del Jugador:");
		System.out.println("->");
		Opciones.nombre = sc.nextLine();
		System.out.println("Tamaño del Tablero:");
		System.out.println("->");
		Opciones.filas = sc.nextInt();
		Opciones.columnas = sc.nextInt();
		System.out.println("Numero de Minas:");
		System.out.println("->");
		Opciones.minas = sc.nextInt();
		Opciones.conf = true;
		
	}

	private static void Ayuda() {

		System.out.println("╔═════════════════════════════════════════════════════════════════════════╗");
		System.out.println("║ El juego consiste en despejar todas las casillas de una pantalla que no ║ ");
		System.out.println("║ oculten una mina. Algunas casillas tienen un número, el cual indica la  ║");
		System.out.println("║ cantidad de minas que hay en las casillas circundantes.                 ║");
		System.out.println("╚═════════════════════════════════════════════════════════════════════════╝");
		System.out.println();
	}

	private static boolean Cuidao() {

		System.out.println();
		System.out.println("Estas a punto de Salir del Juego, estas seguro? ");
		System.out.println("Pulsa 0 para Si / Pulsa 1 Para No");
		int hey = sc.nextInt();
		if (hey == 0) {
			System.out.println("--> Hasta la Proxima <--");
			return false;
		} else {
			System.out.println("--> El Juego Continua <--");
			return true;
		}

	}

	private static void menu() {

		System.out.println();
		System.out.println("---------");
		System.out.println("1. - Mostrar Ayuda");
		System.out.println("2. - Opciones");
		System.out.println("3. - Jugar");
		System.out.println("4. - Ver Ranking");
		System.out.println("0. - Cerrar Programa");
		System.out.println("---------");

	}

	public static void Imprimir(int[][] tableroMinas) {

		System.out.println();
		for (int r = 0; r < tableroMinas.length; r++) {
			System.out.print(r + " ║ ");
			for (int c = 0; c < tableroMinas[0].length; c++) {
				System.out.print(tableroMinas[r][c] + " ");
			}
			System.out.println();
		}
		System.out.print("  ╚═");
		for (int i = 0; i < tableroMinas[0].length; i++) {
			System.out.print("══");
		}
		System.out.println();
		System.out.print("    ");
		for (int i = 0; i < tableroMinas[0].length; i++) {
			System.out.print(i + " ");
		}
		System.out.println();
	}
}
