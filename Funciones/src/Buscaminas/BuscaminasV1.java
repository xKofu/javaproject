package Buscaminas;

import java.util.Random;
import java.util.Scanner;

import javax.annotation.processing.SupportedSourceVersion;

public class BuscaminasV1 {

	static Scanner sc;
	static Random rdom;

	public static void main(String[] args) {

		sc = new Scanner(System.in);
		rdom = new Random();
		
		/*
		 * 
		 *  ESTE NO FUNCIONA
		 * 
		 * 
		 */
		
		
		

		boolean jugar = true; // Flag para el bucle de la partida
		int[] NumerosMat = new int[3]; // Los datos del tablero
		int[] Puntuacion = new int[1]; // Array para guardar la puntuacion
		int[] Coords = new int[2]; // Array para guardar las coordenadas
		Puntuacion[0] = 0; // Pongo la puntuacion a 0
		String PlayerName = "NoName"; // Inicio el nombre del Jugador
		int[][] TableroMinas = null;
		int[][] Tablero = null;
		boolean flag = false;

		System.out.println("╔════════════╗");
		System.out.println("║ BUSCAMINAS ║");
		System.out.println("╚════════════╝");
		System.out.println();

		menu();

		while (jugar) {

			int opcion = sc.nextInt();
			sc.nextLine();

			switch (opcion) {
			case 1:
				Ayuda();
				break;
			case 2:
				PlayerName = Opciones(NumerosMat);
				TableroMinas = new int[NumerosMat[0]][NumerosMat[1]];
				Tablero = new int[NumerosMat[0]][NumerosMat[1]];
				IniciarMinas(TableroMinas, NumerosMat);
				IniciarTablero(Tablero);
				flag = true;
				break;
			case 3:
				if (flag) {
					PedirCoords(Coords);
					jugar = Jugar(Coords, Tablero, TableroMinas, jugar);
				} else {
					System.out.println("Amigo no has generado tablas");
				}
				break;
			case 4:
				VerRanking(PlayerName, Puntuacion);
				break;
			case 5:
				System.out.println("Imprimir");
				Imprimir(Tablero);
				break;
			case 0:
				jugar = Cuidao();
				break;
			}
		}

	}

	private static boolean Jugar(int[] coords, int[][] tablero, int[][] tableroMinas, boolean jugar) {

			
		
		return jugar;

	}

	private static void PedirCoords(int[] coords) {

		System.out.println("Que casilla quieres destapar? ");
		coords[0] = sc.nextInt();
		coords[1] = sc.nextInt();

	}

	private static void IniciarTablero(int[][] tablero) {

		for (int r = 0; r < tablero.length; r++) {
			for (int c = 0; c < tablero[0].length; c++) {
				tablero[r][c] = 9;
			}
		}

		System.out.println("Tablero: ");
		Imprimir(tablero);
		System.out.println();
	}

	private static void VerRanking(String playerName, int[] puntuacion) {

		System.out.println();
		System.out.println("╔═══════════════════════");
		System.out.println("║ -> " + playerName + " tiene una puntuacion de: " + puntuacion[0]);
		System.out.println("╚═══════════════════════");

	}

	private static void IniciarMinas(int[][] tableroMinas, int[] numerosMat) {

		int cont = numerosMat[2];

		while (cont > 0) {
			int r1 = rdom.nextInt(numerosMat[0]);
			int r2 = rdom.nextInt(numerosMat[1]);

			if (tableroMinas[r1][r2] == 1) {

			} else {
				tableroMinas[r1][r2] = 1;
				cont--;
			}
		}
		System.out.println("Minas: ");
		Imprimir(tableroMinas);
		System.out.println();

	}

	private static String Opciones(int[] NumerosMat) {

		// La posicion 0 seran las filas del tablero
		// la posicion 1 seran las columnas del tablero
		// la posicion 2 seran las minas a colocar

		System.out.println();
		System.out.println("Nombre del Jugador:");
		System.out.println("->");
		String Aux = sc.nextLine();
		System.out.println("Tamaño del Tablero:");
		System.out.println("->");
		NumerosMat[0] = sc.nextInt();
		NumerosMat[1] = sc.nextInt();
		System.out.println("Numero de Minas:");
		System.out.println("->");
		NumerosMat[2] = sc.nextInt();

		return Aux;
	}

	private static void Ayuda() {

		System.out.println("╔═════════════════════════════════════════════════════════════════════════╗");
		System.out.println("║ El juego consiste en despejar todas las casillas de una pantalla que no ║ ");
		System.out.println("║ oculten una mina. Algunas casillas tienen un número, el cual indica la  ║");
		System.out.println("║ cantidad de minas que hay en las casillas circundantes.                 ║");
		System.out.println("╚═════════════════════════════════════════════════════════════════════════╝");
		System.out.println();
	}

	private static boolean Cuidao() {

		System.out.println();
		System.out.println("Estas a punto de Salir del Juego, estas seguro? ");
		System.out.println("Pulsa 0 para Si / Pulsa 1 Para No");
		int hey = sc.nextInt();
		if (hey == 0) {
			System.out.println("--> Hasta la Proxima <--");
			return false;
		} else {
			System.out.println("--> El Juego Continua <--");
			return true;
		}

	}

	private static void menu() {

		System.out.println();
		System.out.println("---------");
		System.out.println("1. - Mostrar Ayuda");
		System.out.println("2. - Opciones");
		System.out.println("3. - Jugar");
		System.out.println("4. - Ver Ranking");
		System.out.println("0. - Cerrar Programa");
		System.out.println("---------");

	}

	public static void Imprimir(int[][] tableroMinas) {

		System.out.println();
		for (int r = 0; r < tableroMinas.length; r++) {
			System.out.print(r + " ║ ");
			for (int c = 0; c < tableroMinas[0].length; c++) {
				System.out.print(tableroMinas[r][c] + " ");
			}
			System.out.println();
		}
		System.out.print("  ╚═");
		for (int i = 0; i < tableroMinas[0].length; i++) {
			System.out.print("══");
		}
		System.out.println();
		System.out.print("    ");
		for (int i = 0; i < tableroMinas[0].length; i++) {
			System.out.print(i + " ");
		}
		System.out.println();
	}
}
