package RecursividadJoder;

import java.util.Scanner;

public class Bitlles1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int hey = sc.nextInt();
			
			hey = Bitlles(hey);
			
			System.out.println(hey);

		}
			
	}

	private static int Bitlles(int hey) {
		
		if (hey == 0) {
			return 0;	
		}else if (hey == 1) {
			return 1;
		} else {
			return hey + Bitlles(hey-1);
		}
		
	}
	
	

}
