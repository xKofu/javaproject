package RecursividadJoder;

import java.util.Scanner;

public class OrigenEscacs {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int hey = sc.nextInt();
			
			System.out.println(Func(hey));
		
		}

	}

	private static long Func(int hey) {
		
		if (hey == 1) {
			return 1;
		} else {
			return (long) (Math.pow(2, hey-1) + Func(hey-1));
		}
		
	}

}
