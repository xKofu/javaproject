package RecursividadJoder;

import java.util.Scanner;

public class LaLegoPiramide {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int hey = sc.nextInt();
			
			System.out.println(Func(hey));
			
		}

	}

	private static int Func(int hey) {
	
		if (hey == 1) {
			return 1;
		} else {
			return hey * hey + Func(hey-1);
		}
		
	}

}
