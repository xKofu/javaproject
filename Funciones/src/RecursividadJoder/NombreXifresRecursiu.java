package RecursividadJoder;

import java.util.Scanner;

public class NombreXifresRecursiu {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int hey = sc.nextInt();
			
			hey = Xifres(hey);
			
			System.out.println(hey);

		}
			
	}

	private static int Xifres(int hey) {

		if (hey < 10) {
			return 1;
		} else {
			return 1 + Xifres(hey/10);
		}
		
	}
		
}

