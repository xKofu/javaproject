package RecursividadJoder;

import java.util.Scanner;

public class ComoConejos {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int hey = sc.nextInt();
			
			hey = Fibo(hey);
			
			System.out.println(hey);

		}
		
	}

	private static int Fibo(int hey) {
		
		if (hey <= 2) {
			return 1;
		} else {
			return Fibo(hey-1) + Fibo(hey-2);
		}
		
	}

}
