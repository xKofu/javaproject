package EjerciciosRecursividad;

import java.util.Scanner;

public class RestaDivision {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		
		int hey = sc.nextInt();
		int heyR = sc.nextInt();
		
		hey = Div(hey, heyR);
		
		System.out.println(hey);

	}

	private static int Div(int hey, int heyR) {
		
		if (hey - heyR == 0) {
			return 1;
		} else if (hey - heyR < 0) {
			return 0;
		} else {
			return 1 + Div(hey - heyR, heyR);
		}
		
	}

}
