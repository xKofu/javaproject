package EjerciciosRecursividad;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int hey = sc.nextInt();
		
		hey = Fact(hey);
		
		System.out.println(hey);

	}

	private static int Fact(int hey) {
		
		if (hey == 1) {
			return 1;
		} else {
			return hey * Fact(hey-1);
		}
	
	}

}
